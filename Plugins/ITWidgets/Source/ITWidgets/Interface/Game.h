// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Game.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGame : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class ITWIDGETS_API IGame
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game")
	APawn* GetPlayerPawn_Interface() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Game")
	APlayerController* GetPlayerController_Interface() const;
};
