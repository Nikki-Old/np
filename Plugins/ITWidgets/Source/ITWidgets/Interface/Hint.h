// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Hint.generated.h"

// Forward declaretion:
class AHintBase;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UHint : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */

class ITWIDGETS_API IHint
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "HintInterface")
	bool SetHint(AHintBase* NewHint);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "HintInterface")
	bool RemoveHintByType(EHintType HintType);
};
