// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Frame.generated.h"

class UMenuWidget;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UFrame : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */
class ITWIDGETS_API IFrame
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IFrame")
	void SetFrameSocket(UMenuWidget* Widget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IFrame")
	UMenuWidget* GetFrameSocket() const;
};
