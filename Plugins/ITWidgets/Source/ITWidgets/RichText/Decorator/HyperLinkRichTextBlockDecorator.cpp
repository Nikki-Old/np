// Fill out your copyright notice in the Description page of Project Settings.

#include "RichText/Decorator/HyperLinkRichTextBlockDecorator.h"
#include "Components/RichTextBlock.h"
#include "Widgets/Input/SRichTextHyperlink.h"

#include "Kismet/GameplayStatics.h"

FRichInlineHyperLinkDecorator::FRichInlineHyperLinkDecorator(URichTextBlock* InOwner, UHyperLinkRichTextBlockDecorator* decorator)
	: FRichTextDecorator(InOwner)
{
	if (decorator)
	{
		LinkStyle = decorator->Style;
		OnClickDelegate.BindLambda([=]() {
			decorator->ClickLink();
		});
		Decorator = decorator;

		// OnChangeIdString.BindUFunction(decorator, "SetNewIdString", IdString);
	}
}

bool FRichInlineHyperLinkDecorator::Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const
{
	// <a Id="TEXT">url</>
	if (RunParseResult.Name == TEXT("a") && RunParseResult.MetaData.Contains(TEXT("id")))
	{
		const FTextRange& IdRange = RunParseResult.MetaData[TEXT("id")];

		IdString = Text.Mid(IdRange.BeginIndex, IdRange.EndIndex - IdRange.BeginIndex);
		// OnChangeIdString.ExecuteIfBound();
		// Decorator->IdString = IdString;
		return true;
	}
	return false;
}

TSharedPtr<SWidget> FRichInlineHyperLinkDecorator::CreateDecoratorWidget(const FTextRunInfo& RunInfo, const FTextBlockStyle& TextStyle) const
{
	// const bool bWarnIfMissing = true;

	TSharedPtr<FSlateHyperlinkRun::FWidgetViewModel> model = MakeShareable(new FSlateHyperlinkRun::FWidgetViewModel);

	TSharedPtr<SRichTextHyperlink> link = SNew(SRichTextHyperlink, model.ToSharedRef())
											  .Text(FText::FromString(IdString))
											  .Style(&LinkStyle)
											  .OnNavigate(OnClickDelegate);

	if (link)
	{
		// Hovered:
		FSimpleDelegate HoveredDelegate;
		HoveredDelegate.BindUFunction(Decorator, "HoveredLink", IdString);
		link->SetOnHovered(HoveredDelegate);

		// Unhovered:
		FSimpleDelegate UnhoveredDelegate;
		UnhoveredDelegate.BindUFunction(Decorator, "UnhoveredLink", IdString);
		link->SetOnUnhovered(UnhoveredDelegate);
	}

	return link;
}

UHyperLinkRichTextBlockDecorator::UHyperLinkRichTextBlockDecorator(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer) {}

TSharedPtr<ITextDecorator> UHyperLinkRichTextBlockDecorator::CreateDecorator(URichTextBlock* InOwner)
{
	return MakeShareable(new FRichInlineHyperLinkDecorator(InOwner, this));
}

AGameModeBase* UHyperLinkRichTextBlockDecorator::GetGameMode() const
{
	if (GetWorld())
	{
		return UGameplayStatics::GetGameMode(GetWorld());
	}

	return nullptr;
}

void UHyperLinkRichTextBlockDecorator::SetNewIdString(FString NewString)
{
	IdString = NewString;
}

void UHyperLinkRichTextBlockDecorator::ClickLink_Implementation()
{
	if (GEngine)
	{
		GEngine->AddOnScreenDebugMessage(-1, 20, FColor::Red, IdString);
	}
}

void UHyperLinkRichTextBlockDecorator::HoveredLink_Implementation(const FString& LinkId)
{
}

void UHyperLinkRichTextBlockDecorator::UnhoveredLink_Implementation(const FString& LinkId)
{
}
