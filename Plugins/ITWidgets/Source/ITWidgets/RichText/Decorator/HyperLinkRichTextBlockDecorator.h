// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/RichTextBlockDecorator.h"
#include "HyperLinkRichTextBlockDecorator.generated.h"

/**
 *
 */

class FRichInlineHyperLinkDecorator : public FRichTextDecorator
{
public:
	FRichInlineHyperLinkDecorator(URichTextBlock* InOwner, UHyperLinkRichTextBlockDecorator* decorator);

	/** Find in Rich text: */
	virtual bool Supports(const FTextRunParseResults& RunParseResult, const FString& Text) const override;

protected:
	virtual TSharedPtr<SWidget> CreateDecoratorWidget(const FTextRunInfo& RunInfo, const FTextBlockStyle& TextStyle) const override;
	FHyperlinkStyle LinkStyle;
	FSimpleDelegate OnClickDelegate;
	mutable FString IdString = "";

private:
	FSimpleDelegate OnChangeIdString;
	UHyperLinkRichTextBlockDecorator* Decorator = nullptr;
};

//////////////////////////////////////////////////////////////////////////

UCLASS()
class ITWIDGETS_API UHyperLinkRichTextBlockDecorator : public URichTextBlockDecorator
{
	GENERATED_BODY()

public:
	UHyperLinkRichTextBlockDecorator(const FObjectInitializer& ObjectInitializer);

	virtual TSharedPtr<ITextDecorator> CreateDecorator(URichTextBlock* InOwner) override;

	/** Link style: */
	UPROPERTY(EditAnywhere, Category = Appearance)
	FHyperlinkStyle Style;

	UPROPERTY(VisibleanyWhere, BlueprintReadOnly, Category = "Hyperlink")
	FString IdString = "";

	UFUNCTION(BlueprintNativeEvent)
	void ClickLink();

	UFUNCTION(BlueprintNativeEvent)
	void HoveredLink(const FString& LinkId);

	UFUNCTION(BlueprintNativeEvent)
	void UnhoveredLink(const FString& LinkId);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Hyperlink")
	AGameModeBase* GetGameMode() const;

	/** Set new IdString: */
	UFUNCTION()
	void SetNewIdString(FString NewString);
};
