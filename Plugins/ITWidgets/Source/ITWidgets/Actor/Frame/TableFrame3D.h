// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Actor/Frame/Frame3D.h"
#include "TableFrame3D.generated.h"

class UDataTable;
/**
 *
 */

UCLASS()
class ITWIDGETS_API ATableFrame3D : public AFrame3D
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "TableFrame3D")
	void ShowWidget(const FName& RowName, float Time = 0.0f);
	void ShowWidget_Implementation(const FName& RowName, float Time = 0.0f);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "TableFrame3D")
	void UnShowWidget();
	void UnShowWidget_Implementation();

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "TableFrame3D")
	UDataTable* WidgetsInfoTable = nullptr;
};
