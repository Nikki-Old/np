// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/Frame/Icon/IconFrame3D.h"
#include "Components/WidgetComponent.h"
#include "Actor/Frame/Frame3D.h"
#include "IconFrame3D.h"
#include "Widget/WidgetBase.h"
#include "Interface/Frame.h"
#include "Widget/Menu/MenuWidget.h"
#include "Widget/Frame/FrameWidget.h"

// Sets default values
AIconFrame3D::AIconFrame3D()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create FrameMesh:
	FrameMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Frame"));
	FrameMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FrameMesh->SetRelativeScale3D(FVector(1.0f, 0.2f, 0.3f));
	this->SetRootComponent(FrameMesh);

	// Create IconWidgetComp:
	IconWidgetComp = CreateDefaultSubobject<UWidgetComponent>(TEXT("IconWidgetComponent"));
	IconWidgetComp->SetRelativeScale3D(FVector(1.0f, 0.055f, 0.033f));
	IconWidgetComp->SetupAttachment(FrameMesh);
}

// Called when the game starts or when spawned
void AIconFrame3D::BeginPlay()
{
	Super::BeginPlay();

	Initialize();
}

void AIconFrame3D::FrameSwitchSocket_Implementation(UMenuWidget* FrameWidget)
{
}

void AIconFrame3D::FrameWidgetIsReady_Implementation(UWidgetBase* FrameWidget)
{
	if (FrameWidget)
	{
		FrameWidget->OnReadyWidget.RemoveDynamic(this, &AIconFrame3D::FrameWidgetIsReady);

		auto FrameSocket = IFrame::Execute_GetFrameSocket(FrameWidget);
		if (FrameSocket)
		{
			FrameSocket->OnClickAnyButton.AddDynamic(this, &AIconFrame3D::MenuCommands);
		}
	}
}

void AIconFrame3D::MenuCommands_Implementation(FName ButtonName)
{
	// BP
}

void AIconFrame3D::Initialize_Implementation()
{
	// Find parent frame:
	ParentFrame = Cast<AFrame3D>(this->GetParentActor());
	if (ParentFrame)
	{
		const auto FrameWidget = ParentFrame->GetFrameWidget();
		if (FrameWidget)
		{
			FrameWidget->OnNewFrameSocket.AddDynamic(this, &AIconFrame3D::FrameSwitchSocket);
			FrameWidget->OnReadyWidget.AddDynamic(this, &AIconFrame3D::FrameWidgetIsReady);
		}
	}
}

// Called every frame
void AIconFrame3D::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
