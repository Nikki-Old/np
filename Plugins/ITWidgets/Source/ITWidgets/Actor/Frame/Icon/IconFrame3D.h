// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "IconFrame3D.generated.h"

// Forward declaration:
class UStaticMeshComponent;
class UWidgetComponent;
class AFrame3D;

UCLASS()
class ITWIDGETS_API AIconFrame3D : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AIconFrame3D();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "IconFrame3D")
	AFrame3D* GetParentFrame() const { return ParentFrame; }

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "IconFrame3D")
	UStaticMeshComponent* FrameMesh = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "IconFrame3D")
	UWidgetComponent* IconWidgetComp = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Initialize function:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IconFrame3D")
	void Initialize();

	UFUNCTION(BlueprintNativeEvent, Category = "IconFrame3D")
	void FrameWidgetIsReady(UWidgetBase* FrameWidget);

	UFUNCTION(BlueprintNativeEvent, Category = "IconFrame3D")
	void FrameSwitchSocket(UMenuWidget* FrameWidget);

	UPROPERTY(BlueprintReadOnly, Category = "IconFrame3D")
	AFrame3D* ParentFrame = nullptr;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IconFrame3D")
	void MenuCommands(FName ButtonName);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
