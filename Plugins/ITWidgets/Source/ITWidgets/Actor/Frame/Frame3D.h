// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Frame3D.generated.h"

// Forward declaration:
class UWidgetComponent;
class UWidgetBase;
class UFrameWidget;
class USpringArmComponent;

/** Plane type */
UENUM()
enum class EPlaneType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	Horizontal_Type UMETA(DisplayName = "Horizontal"),
	Vertical_Type UMETA(DisplayName = "Vertical")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnOpen, AFrame3D*, Frame3D);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnClose, AFrame3D*, Frame3D);

UCLASS()
class ITWIDGETS_API AFrame3D : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFrame3D(const FObjectInitializer& ObjectInitializer);

#pragma region Component

	/** Frame Mesh component - Root component: */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Frame3D")
	UStaticMeshComponent* FrameMesh = nullptr;

	static FName FrameMeshName;

	/** Widget component */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Frame3D")
	UWidgetComponent* WidgetComp = nullptr;

#pragma endregion

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Frame3D")
	UFrameWidget* GetFrameWidget() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Frame3D")
	void SetFrameVisibility(bool bIsNewVisible);
	void SetFrameVisibility_Implementation(bool bIsNewVisible);

#pragma region Open /Close
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Frame3D | Open/Close")
	void Open();
	void Open_Implementation();

	/** Delegate for broadcast: "On Open" */
	UPROPERTY(BlueprintAssignable, Category = "Frame3D | Open/Close")
	FOnOpen OnOpen;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Frame3D | Open/Close")
	void Close();
	void Close_Implementation();

	/** Delegate for broadcast: "On Close" */
	UPROPERTY(BlueprintAssignable, Category = "Frame3D | Open/Close")
	FOnClose OnClose;

#pragma endregion

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

#pragma region Plane
public:
	UFUNCTION(BlueprintCallable, Category = "Frame3D | Plane")
	void SetPlane(EPlaneType NewPlaneType);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Frame3D | Plane")
	FRotator WidgetRotation_Horizontal = FRotator(0);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Frame3D | Plane")
	FRotator MeshRotation_Horizontal = FRotator(0);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Frame3D | Plane")
	FRotator WidgetRotation_Vertical = FRotator(0);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Frame3D | Plane")
	FRotator MeshRotation_Vertical = FRotator(0);

private:
	EPlaneType CurrentPlaneType = EPlaneType::None_Type;

#pragma endregion

protected:
	UFUNCTION(BlueprintCallable, Category = "Frame3D")
	void StartRotationToTarget(AActor* TargetToRotation);

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Frame3D | Rotation")
	float HowOftenUpdateRotation = 0.01f;

	UFUNCTION(BlueprintNativeEvent, Category = "Frame3D | Rotation")
	void UpdateRotation();
	void UpdateRotation_Implementation();

	UPROPERTY(BlueprintReadOnly, Category = "Frame3D", meta = (ExposeOnSpawn = true))
	USpringArmComponent* ParentSpringArm = nullptr;

private:
	/** Rotation timer */
	UPROPERTY()
	FTimerHandle RotationTimer;

	UPROPERTY()
	AActor* CurrentTargetRotation = nullptr;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	bool bIsVisible = true;
};
