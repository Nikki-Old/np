// Fill out your copyright notice in the Description page of Project Settings.

#include "Frame3D.h"
#include "Components/WidgetComponent.h"
#include "Widget/WidgetBase.h"
#include "Widget/Frame/FrameWidget.h"

#include "Kismet/KismetMathLibrary.h"

FName AFrame3D::FrameMeshName(TEXT("FrameMesh"));

// Sets default values
AFrame3D::AFrame3D(const FObjectInitializer& ObjectInitializer)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

#pragma region MeshComponent

	FrameMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FrameMesh"));
	this->SetRootComponent(FrameMesh);

#pragma endregion

#pragma region WidgetComponent

	WidgetComp = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetComponent"));
	WidgetComp->SetupAttachment(FrameMesh);

	// In VR only work WORLD SPCACE:
	WidgetComp->SetWidgetSpace(EWidgetSpace::World);
	WidgetComp->SetRelativeScale3D(FVector(0.015f, 0.015f, 0.015f));
	WidgetComp->SetDrawSize(FVector2D(2000.0f, 1000.0f));

#pragma endregion
}

UFrameWidget* AFrame3D::GetFrameWidget() const
{
	if (WidgetComp)
	{
		return Cast<UFrameWidget>(WidgetComp->GetWidget());
	}

	return nullptr;
}

void AFrame3D::SetFrameVisibility_Implementation(bool bIsNewVisible)
{
	if (bIsVisible == bIsNewVisible)
	{
		// do nothing:
		return;
	}

	bIsVisible = bIsNewVisible;
	// and toggle collision:

	const auto NewCollision = bIsVisible ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision;

	auto Components = this->GetComponents();
	for (auto Component : Components)
	{
		auto PrimitiveComponent = Cast<UPrimitiveComponent>(Component);
		if (PrimitiveComponent)
		{
			PrimitiveComponent->SetVisibility(bIsVisible, true);
			PrimitiveComponent->SetCollisionEnabled(NewCollision);
		}
	}
}

void AFrame3D::Open_Implementation()
{
	SetFrameVisibility(true);
	OnOpen.Broadcast(this);
}

void AFrame3D::Close_Implementation()
{
	SetFrameVisibility(false);
	OnClose.Broadcast(this);
}

void AFrame3D::SetPlane(EPlaneType NewPlaneType)
{
	if (CurrentPlaneType == NewPlaneType)
	{
		// Do nothing...
		return;
	}

	CurrentPlaneType = NewPlaneType;

	if (FrameMesh)
	{
		FrameMesh->SetRelativeRotation(CurrentPlaneType == EPlaneType::Horizontal_Type ? MeshRotation_Horizontal : MeshRotation_Vertical);
	}

	if (WidgetComp)
	{
		WidgetComp->SetRelativeRotation(CurrentPlaneType == EPlaneType::Horizontal_Type ? WidgetRotation_Horizontal : WidgetRotation_Vertical);
	}
}

// Called when the game starts or when spawned
void AFrame3D::BeginPlay()
{
	Super::BeginPlay();
}

void AFrame3D::StartRotationToTarget(AActor* TargetToRotation)
{
	if (TargetToRotation)
	{
		CurrentTargetRotation = TargetToRotation;

		if (GetWorld())
		{
			if (RotationTimer.IsValid())
			{
				GetWorld()->GetTimerManager().ClearTimer(RotationTimer);
			}

			GetWorld()->GetTimerManager().SetTimer(RotationTimer, this, &AFrame3D::UpdateRotation, HowOftenUpdateRotation, true);
		}
	}
}

void AFrame3D::UpdateRotation_Implementation()
{
	const auto TargetRotation = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), CurrentTargetRotation->GetActorLocation());
	if (!TargetRotation.IsZero())
	{
		UE_LOG(LogTemp, Warning, TEXT("Target Rotation: %s"), *TargetRotation.ToString());
		this->SetActorRotation(TargetRotation);
	}
}

// Called every frame
void AFrame3D::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
