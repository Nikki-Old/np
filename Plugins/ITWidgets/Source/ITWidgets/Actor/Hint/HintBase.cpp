// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/Hint/HintBase.h"

#include "GameFramework/SpringArmComponent.h"
#include "ActorComponent/HintManagerComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogHintBase, All, All);

// Sets default values
AHintBase::AHintBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(SceneComponent);
}

bool AHintBase::SetTextByName_Implementation(FName KeyName)
{
	return false;
}

// Called when the game starts or when spawned
void AHintBase::BeginPlay()
{
	Super::BeginPlay();

	InitializeHint();
}

void AHintBase::InitializeHint_Implementation()
{
	const auto ChildActorComponent = GetRootComponent()->GetAttachParent();

	if (ChildActorComponent)
	{
		if (ChildActorComponent->GetOwner())
		{
			HintManager = Cast<UHintManagerComponent>(ChildActorComponent->GetOwner()->FindComponentByClass(UHintManagerComponent::StaticClass()));
		}

		if (HintManager)
		{
			HintManager->AddHintInOwner(this);
		}
		else
		{
			UE_LOG(LogHintBase, Error, TEXT("%s not find HintManager in %s"), *this->GetName(), *GetOwner()->GetName());
		}

		bool bIsFindSpringArm = false;

		ParentSpringArm = Cast<USpringArmComponent>(ChildActorComponent->GetAttachParent());
		if (ParentSpringArm)
		{
			DefaultArmLength = ParentSpringArm->TargetArmLength;
			ParentSpringArm->TargetArmLength = 0.0f;
			bIsFindSpringArm = true;
		}

		this->ToggleVisible(false);
	}
}

// Called every frame
void AHintBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AHintBase::ChangeState_Implementation(EHintState NewState)
{
	if (CurrentHintState == NewState)
	{
		return;
	}

	CurrentHintState = NewState;
}

void AHintBase::ToggleVisible_Implementation(bool bIsVisible)
{
	if (this->GetRootComponent()->IsVisible() != bIsVisible)
	{
		this->SetActorEnableCollision(bIsVisible);
		GetRootComponent()->SetVisibility(bIsVisible, true);
	}
}

void AHintBase::StartShow_Implementation(bool bIsShow)
{
	StartChangeArmLength(bIsShow);
}

void AHintBase::StartChangeArmLength(bool bIsUp)
{
	if (!GetWorld() || !ParentSpringArm)
	{
		return;
	}

	bIsUpArmLength = bIsUp;

	ToggleVisible(true);
	if (!ArmLengthTimer.IsValid())
	{
		GetWorld()->GetTimerManager().SetTimer(ArmLengthTimer, this, &AHintBase::ChangeArmLength, InRateArmLengthTimer, true);
	}
}

void AHintBase::ChangeArmLength()
{
	if (ParentSpringArm)
	{
		float CurrentArmLength = ParentSpringArm->TargetArmLength;

		float TempOffset = 0.0f;
		if (bIsUpArmLength)
		{
			TempOffset = OffsetArmLength;
		}
		else
		{
			TempOffset = OffsetArmLength * -1;
		}

		// UE_LOG(LogTemp, Warning, TEXT("CurrentArmLength: %f || TempOffset: %f"), CurrentArmLength, TempOffset);

		// Set new target arm length:
		ParentSpringArm->TargetArmLength = FMath::Clamp(CurrentArmLength + TempOffset, 0.0f, DefaultArmLength);

		if (ParentSpringArm->TargetArmLength <= 0.0f)
		{
			ToggleVisible(false);
			GetWorld()->GetTimerManager().ClearTimer(ArmLengthTimer);
		}

		if (ParentSpringArm->TargetArmLength >= DefaultArmLength)
		{
			GetWorld()->GetTimerManager().ClearTimer(ArmLengthTimer);
		}
	}
}
