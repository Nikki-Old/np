// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "HintBase.generated.h"

class UHintManagerComponent;
class USpringArmComponent;

/** Hint type: */
UENUM(BlueprintType)
enum class EHintType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	Base_Type UMETA(DisplayName = "Base"),
	Definition_Type UMETA(DisplayName = "Definition"),
	MAX_Type UMETA(DisplayName = "MAX")
};

/** Hint state: */
UENUM(BlueprintType)
enum class EHintState : uint8
{
	None_State UMETA(DisplayName = "None"),
	Open_State UMETA(DisplayName = "Open"),
	Close_State UMETA(DisplayName = "Close")
};

/** Hint class base */
UCLASS()
class ITWIDGETS_API AHintBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AHintBase();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Hint")
	FORCEINLINE FName GetHintKeyName() const { return HintKeyName; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Hint")
	FORCEINLINE EHintState GetCurrentHintState() const { return CurrentHintState; }

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hint")
	bool SetTextByName(FName KeyName);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Hint")
	FName HintKeyName = FName();

#pragma region Component

protected:
	/** Base root component */
	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "Hint")
	USceneComponent* SceneComponent = nullptr;

#pragma endregion

public:
	/** Initialize event in BeginPlay */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hint")
	void InitializeHint();
	void InitializeHint_Implementation();

protected:
	/** Get spring arm which attached:  */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Hint")
	FORCEINLINE USpringArmComponent* GetParentSpringArm() const { return ParentSpringArm; }

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Reaction to FOnChangeHintState delegate in UHintManagerComponent */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hint")
	void ChangeState(EHintState NewState);
	void ChangeState_Implementation(EHintState NewState);

	/** Set visibility and collision */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hint")
	void ToggleVisible(bool bIsVisible);
	void ToggleVisible_Implementation(bool bIsVisible);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Hint")
	void StartShow(bool bIsShow);
	void StartShow_Implementation(bool bIsShow);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Hint")
	EHintType GetHintType() const { return HintType; }

private:
	/** Who controlled the HintBase */
	UPROPERTY(VisibleAnyWhere, Category = "Hint")
	UHintManagerComponent* HintManager = nullptr;

	/** Spring arm which attached */
	UPROPERTY()
	USpringArmComponent* ParentSpringArm = nullptr;

#pragma region ArmLength
protected:
	UFUNCTION(BlueprintCallable, Category = "Hint | ArmLength")
	void StartChangeArmLength(bool bIsUp);

	UPROPERTY(EditDefaultsOnly, Category = "Hint | ArmLength")
	float InRateArmLengthTimer = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = "Hint | ArmLength")
	float OffsetArmLength = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Hint", meta = (ExposeOnSpawn = true))
	EHintType HintType = EHintType::None_Type;

private:
	UFUNCTION()
	void ChangeArmLength();

	FTimerHandle ArmLengthTimer;

	float DefaultArmLength = 0.0f;

	/** Set direction change ArmLength */
	bool bIsUpArmLength = false;

	UPROPERTY()
	EHintState CurrentHintState = EHintState::None_State;

#pragma endregion
};
