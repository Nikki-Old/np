// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Menu/ContentMenuWidget.h"
#include "Widget/Button/ButtonWidget.h"
#include "Widget/ContentWidget/ContentWidgetBase.h"

bool UContentMenuWidget::Initialize()
{
	return Super::Initialize();
}

void UContentMenuWidget::NativeConstruct()
{

	BindOnInfoSheet();

	Super::NativeConstruct();
}

void UContentMenuWidget::SwitchContent_Implementation(FName ButtonName)
{
	if (ButtonName.IsNone())
	{
		return;
	}

	if (ContentWidget)
	{
		ENextSheetType NextSheetType = ENextSheetType::None_Type;

		if (ButtonName == "Previous")
		{
			NextSheetType = ENextSheetType::Previous_Type;
		}
		else
		{
			NextSheetType = ENextSheetType::Next_Type;
		}

		ContentWidget->SetInfoSheet(0, NextSheetType);
	}
}

void UContentMenuWidget::SetVisibilityButton_Implementation(UButtonWidget* ButtonWidget, bool bIsVisible)
{
	if (ButtonWidget)
	{
		ESlateVisibility NewSlateVisibility = ESlateVisibility::Hidden;

		if (bIsVisible)
		{
			NewSlateVisibility = ESlateVisibility::Visible;
		}

		ButtonWidget->SetVisibility(NewSlateVisibility);
	}
}

void UContentMenuWidget::BindOnInfoSheet()
{
	if (NextButton)
	{
		FOnClickButton OnClickButton;
		OnClickButton.BindUFunction(this, "SwitchContent");
		NextButton->BindOnClick(OnClickButton);

		if (NextButton->GetKeyName().IsNone())
		{
			NextButton->SetKeyName("Next");
		}
	}

	if (PreviousButton)
	{
		FOnClickButton OnClickButton;
		OnClickButton.BindUFunction(this, "SwitchContent");
		PreviousButton->BindOnClick(OnClickButton);

		if (PreviousButton->GetKeyName().IsNone())
		{
			PreviousButton->SetKeyName("Previous");
		}
	}

	if (ContentWidget)
	{
		ContentWidget->OnFirstInfoSheet.AddUniqueDynamic(this, &UContentMenuWidget::PreviousButtonIsCollapsed);
		ContentWidget->OnLastInfoSheet.AddUniqueDynamic(this, &UContentMenuWidget::NextButtonIsCollapsed);
		ContentWidget->OnMiddleInfoSheet.AddUniqueDynamic(this, &UContentMenuWidget::AllButtonIsVisisble);

		const auto Index = ContentWidget->GetCurrentInfoIndex();
		if (Index == 0)
		{
			PreviousButtonIsCollapsed();
		}
		else if (Index == ContentWidget->GetNumInfos() - 1)
		{
			NextButtonIsCollapsed();
		}
	}
}

void UContentMenuWidget::PreviousButtonIsCollapsed()
{
	SetVisibilityButton(PreviousButton, false);
	SetVisibilityButton(NextButton, true);
}

void UContentMenuWidget::NextButtonIsCollapsed()
{
	SetVisibilityButton(PreviousButton, true);
	SetVisibilityButton(NextButton, false);
}

void UContentMenuWidget::AllButtonIsVisisble()
{
	SetVisibilityButton(PreviousButton, true);
	SetVisibilityButton(NextButton, true);
}
