// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widget/Menu/MenuWidget.h"
#include "ContentMenuWidget.generated.h"

/**
 *
 */

class UButtonWidget;
class UContentWidgetBase;

UCLASS()
class ITWIDGETS_API UContentMenuWidget : public UMenuWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ContentMenu", meta = (BindWidget))
	UContentWidgetBase* ContentWidget = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ContentMenu", meta = (BindWidget))
	UButtonWidget* NextButton = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ContentMenu", meta = (BindWidget))
	UButtonWidget* PreviousButton = nullptr;

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "ContentMenu")
	void SwitchContent(FName ButtonName);
	void SwitchContent_Implementation(FName ButtonName);

	UFUNCTION(BlueprintNativeEvent, Category = "ContentMenu")
	void SetVisibilityButton(UButtonWidget* ButtonWidget, bool bIsVisible);
	void SetVisibilityButton_Implementation(UButtonWidget* ButtonWidget, bool bIsVisible);

private:
	UFUNCTION()
	void BindOnInfoSheet();

	UFUNCTION()
	void PreviousButtonIsCollapsed();
	UFUNCTION()
	void NextButtonIsCollapsed();
	UFUNCTION()
	void AllButtonIsVisisble();
};
