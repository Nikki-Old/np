// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuWidget.h"
#include "Widget/Button/ButtonWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Interface/Frame.h"
#include "Widget/Menu/MenuWidget.h"

bool UMenuWidget::Initialize()
{
	return Super::Initialize();
}

void UMenuWidget::NativeConstruct()
{
	if (BackButton)
	{
		FOnClickButton OnClickButton;
		OnClickButton.BindUFunction(this, "OnBackEvent"); // UMenuWidget::OnBackEvent_Implementation);

		BackButton->BindOnClick(OnClickButton);
	}

	BindAllButtons();
	Super::NativeConstruct();
}

void UMenuWidget::Show_Implementation(UUserWidget* NewPreviousWidget)
{
	if (NewPreviousWidget)
	{
		if (!PreviousWidget)
		{
			// Save...
			PreviousWidget = NewPreviousWidget;
			OldPreviousWidgetVisibility = PreviousWidget->GetVisibility();

			// Collapsed...
			PreviousWidget->SetVisibility(ESlateVisibility::Collapsed);
		}
		else
		{
			NewPreviousWidget->SetVisibility(ESlateVisibility::Collapsed);
			this->SetVisibility(ESlateVisibility::Visible);
		}
	}
}

void UMenuWidget::Unshow_Implementation()
{
	this->SetVisibility(ESlateVisibility::Collapsed);

	if (PreviousWidget)
	{
		PreviousWidget->SetVisibility(OldPreviousWidgetVisibility);

		if (PreviousWidget->IsA(UMenuWidget::StaticClass()))
		{
			auto MenuWidget = Cast<UMenuWidget>(PreviousWidget);
			if (MenuWidget)
			{
				auto Parent = MenuWidget->GetFrameParent();

				if (Parent)
				{
					if (Parent->GetClass()->ImplementsInterface(UFrame::StaticClass()))
					{
						IFrame::Execute_SetFrameSocket(Parent, MenuWidget);
					}
				}
			}
		}
	}

	this->RemoveFromParent();
}

void UMenuWidget::SetFrameSocket_Implementation(UMenuWidget* Widget)
{
	FrameWidget = Widget;
}

UMenuWidget* UMenuWidget::GetFrameSocket_Implementation() const
{
	return FrameWidget;
}

void UMenuWidget::OnBackEvent_Implementation()
{
	Unshow();
}

void UMenuWidget::BindAllButtons()
{
	if (this->WidgetTree)
	{
		TArray<UWidget*> Widgets = {};
		this->WidgetTree->GetAllWidgets(Widgets);

		for (auto Widget : Widgets)
		{
			if (Widget->IsA(UButtonWidget::StaticClass()) && Widget != BackButton)
			{
				auto Button = Cast<UButtonWidget>(Widget);
				if (Button && !Button->GetKeyName().IsNone())
				{
					HoveredButtons.Add(Button->GetKeyName(), false);

					FOnClickButton OnClickButton;
					OnClickButton.BindUFunction(this, "AnyButtonIsClick");
					Button->BindOnClick(OnClickButton);

					FOnHoveredButton OnHoveredButton;
					OnHoveredButton.BindUFunction(this, "AnyButtonIsHovered");
					Button->BindOnHovered(OnHoveredButton);

					FOnUnhoveredButton OnUnhoveredButton;
					OnUnhoveredButton.BindUFunction(this, "AnyButtonIsUnhovered");
					Button->BindOnUnhovered(OnUnhoveredButton);
				}
			}
		}
	}
}

void UMenuWidget::AnyButtonIsClick(FName ButtonName)
{
	if (!bAnyButtonIsClick)
	{
		if (!ButtonName.IsNone())
		{
			bAnyButtonIsClick = false;
			OnClickAnyButton.Broadcast(ButtonName);
		}
	}
}

void UMenuWidget::AnyButtonIsHovered(FName ButtonName)
{
	if (!bAnyButtonIsClick)
	{
		if (!ButtonName.IsNone())
		{
			bool IsHovered = HoveredButtons[ButtonName];
			if (!IsHovered)
			{
				HoveredButtons[ButtonName] = !IsHovered;

				bAnyButtonIsClick = false;
				OnHoveredAnyButton.Broadcast(ButtonName);
			}
		}
	}
}

void UMenuWidget::AnyButtonIsUnhovered(FName ButtonName)
{
	if (!bAnyButtonIsClick)
	{
		if (!ButtonName.IsNone())
		{
			bool IsHovered = HoveredButtons[ButtonName];
			if (IsHovered)
			{
				HoveredButtons[ButtonName] = !IsHovered;
				bAnyButtonIsClick = false;
				OnUnhoveredAnyButton.Broadcast(ButtonName);
			}
		}
	}
}
