// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widget/Menu/MenuWidget.h"
#include "MenuChooseContent.generated.h"

// Forward declaration:
class UButtonWithTextWidget;
class UContentWidgetBase;
class UPanelWidget;

/**
 *
 */

UCLASS()
class ITWIDGETS_API UMenuChooseContent : public UMenuWidget
{
	GENERATED_BODY()

public:
	// Override defaults methods:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	/** Widget with all content: */
	UPROPERTY(BlueprintReadWrite, Category = "ContentMenu", meta = (BindWidget))
	UContentWidgetBase* ContentWidget = nullptr;

	UPROPERTY(BlueprintReadWrite, Category = "ContentMenu", meta = (BindWidget))
	UPanelWidget* PanelForButtons = nullptr;

	UFUNCTION(BlueprintCallable, Category = "ContentMenu")
	void SetContent(TArray<UWidget*> NewContent);

protected:
	UFUNCTION()
	void SwapSheetInfo(FName KeyName);

	UFUNCTION()
	void CreateButtonsForContent();

	UFUNCTION(BlueprintNativeEvent, Category = "ContentMenu")
	void AddButtonInPanel(UButtonWidget* NewButton, int32 IndexButton);
	void AddButtonInPanel_Implementation(UButtonWidget* NewButton, int32 IndexButton);

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ContentMenu")
	TSubclassOf<UButtonWithTextWidget> BaseButtonClass = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "ContentMenu")
	TSubclassOf<UButtonWidget> SwapButtonClass = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "ContentMenu")
	UButtonWidget*			   LeftSwapButton = nullptr;
	UPROPERTY(BlueprintReadOnly, Category = "ContentMenu")
	UButtonWidget*			   RightSwapButton = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "ContentMenu")
	TArray<UButtonWidget*> Buttons = {};

	UPROPERTY(BlueprintReadOnly, Category = "ContentMenu")
	int32 MaxButtons = 3;

	UFUNCTION(BlueprintNativeEvent, Category = "ContentMenu")
	void OnClickChooseButton(FName ButtonName);
	void OnClickChooseButton_Implementation(FName ButtonName);
};
