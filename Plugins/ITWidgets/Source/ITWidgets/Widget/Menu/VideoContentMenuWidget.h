// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widget/Menu/ContentMenuWidget.h"
#include "VideoContentMenuWidget.generated.h"

/**
 *
 */

class UButtonWidget;
class UVideoContentWidget;

UCLASS()
class ITWIDGETS_API UVideoContentMenuWidget : public UContentMenuWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "VideoContentMenu")
	UVideoContentWidget* GetVideoContentWidget() const { return VideoContentWidget; }

protected:
	UPROPERTY(meta = (BindWidget))
	UButtonWidget* StopButton = nullptr;

	UPROPERTY(meta = (BindWidget))
	UButtonWidget* PlayButton = nullptr;

	UPROPERTY(meta = (BindWidget))
	UButtonWidget* PauseButton = nullptr;

	UFUNCTION(Category = "VideoContentMenu")
	void Stop();

	UFUNCTION(Category = "VideoContentMenu")
	void Play();

	UFUNCTION(Category = "VideoContentMenu")
	void Pause();

private:
	UPROPERTY()
	UVideoContentWidget* VideoContentWidget = nullptr;
};
