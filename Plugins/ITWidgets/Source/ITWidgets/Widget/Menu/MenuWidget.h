// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widget/WidgetBase.h"
#include "Interface/Frame.h"
#include "MenuWidget.generated.h"

/**
 *
 */

// Forward declaration:
class UButtonWidget;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnClickAnyButton, FName, ButtonName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHoveredAnyButton, FName, ButtonName);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUnhoveredAnyButton, FName, ButtonName);

UCLASS()
class ITWIDGETS_API UMenuWidget : public UWidgetBase, public IFrame
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MenuWidget")
	void Show(UUserWidget* NewPreviousWidget);
	void Show_Implementation(UUserWidget* NewPreviousWidget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "MenuWidget")
	void Unshow();
	void Unshow_Implementation();

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IFrame")
	void SetFrameSocket(UMenuWidget* Widget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IFrame")
	UMenuWidget* GetFrameSocket() const;

#pragma region Delegate

	UPROPERTY(BlueprintAssignable, Category = "MenuWidget")
	FOnClickAnyButton OnClickAnyButton;

	UPROPERTY(BlueprintAssignable, Category = "MenuWidget")
	FOnHoveredAnyButton OnHoveredAnyButton;

	UPROPERTY(BlueprintAssignable, Category = "MenuWidget")
	FOnUnhoveredAnyButton OnUnhoveredAnyButton;

#pragma endregion

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void SetFrameParent(UUserWidget* Frame) { FrameParent = Frame; }

	UFUNCTION(BlueprintCallable)
	UUserWidget* GetFrameParent() const { return FrameParent; }

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "MenuWidget", meta = (BindWidget))
	UButtonWidget* BackButton = nullptr;

	UFUNCTION(BlueprintNativeEvent, Category = "MenuWidget")
	void OnBackEvent();
	void OnBackEvent_Implementation();

	UPROPERTY()
	UUserWidget* PreviousWidget = nullptr;
	ESlateVisibility OldPreviousWidgetVisibility = ESlateVisibility::Visible;

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void BindAllButtons();

protected:
	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void AnyButtonIsClick(FName ButtonName);

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void AnyButtonIsHovered(FName ButtonName);

	UFUNCTION(BlueprintCallable, Category = "MenuWidget")
	void AnyButtonIsUnhovered(FName ButtonName);

private:
	UPROPERTY()
	TMap<FName, bool> HoveredButtons;

	bool bAnyButtonIsClick = false;

	UPROPERTY()
	UUserWidget* FrameParent = nullptr;

	UPROPERTY()
	UMenuWidget* FrameWidget = nullptr;
};
