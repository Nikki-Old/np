// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Menu/VideoContentMenuWidget.h"
#include "Widget/Button/ButtonWidget.h"
#include "Widget/ContentWidget/ContentWidgetBase.h"
#include "Widget/ContentWidget/VideoContentWidget.h"

bool UVideoContentMenuWidget::Initialize()
{
	return Super::Initialize();
}

void UVideoContentMenuWidget::NativeConstruct()
{
	if (StopButton)
	{
		FOnClickButton StopClick;
		StopClick.BindUFunction(this, "Stop");
		StopButton->BindOnClick(StopClick);
	}

	if (PlayButton)
	{
		FOnClickButton PlayClick;
		PlayClick.BindUFunction(this, "Play");
		PlayButton->BindOnClick(PlayClick);
	}

	if (PauseButton)
	{
		FOnClickButton PauseClick;
		PauseClick.BindUFunction(this, "Pause");
		PauseButton->BindOnClick(PauseClick);
	}

	if (ContentWidget)
	{
		VideoContentWidget = Cast<UVideoContentWidget>(ContentWidget);

		check(VideoContentWidget);

		if (VideoContentWidget)
		{
		}
	}

	Super::NativeConstruct();
}

void UVideoContentMenuWidget::Stop()
{
	if (VideoContentWidget)
	{
		VideoContentWidget->StopVideo();
	}
}

void UVideoContentMenuWidget::Play()
{
	if (VideoContentWidget)
	{
		VideoContentWidget->PlayVideo();
	}
}

void UVideoContentMenuWidget::Pause()
{
	if (VideoContentWidget)
	{
		VideoContentWidget->SetPauseVideo(true);
	}
}
