// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Menu/MenuChooseContent.h"
#include "Widget/ContentWidget/ContentWidgetBase.h"
#include "Widget/Button/ButtonWithTextWidget.h"

#include "Blueprint/WidgetTree.h"
#include "Components/PanelWidget.h"

bool UMenuChooseContent::Initialize()
{

	return Super::Initialize();
}

void UMenuChooseContent::NativeConstruct()
{
	Super::NativeConstruct();

	this->OnClickAnyButton.AddDynamic(this, &UMenuChooseContent::SwapSheetInfo);
}

void UMenuChooseContent::SetContent(TArray<UWidget*> NewContent)
{
	if (ContentWidget)
	{
		ContentWidget->SetContents(NewContent);
	}

	CreateButtonsForContent();
}

void UMenuChooseContent::SwapSheetInfo(FName KeyName)
{
	if (!ContentWidget)
	{
		return;
	}

	if (KeyName == "Left")
	{
		ContentWidget->SetInfoSheet(0, ENextSheetType::Previous_Type);
	}

	if (KeyName == "Right")
	{
		ContentWidget->SetInfoSheet(0, ENextSheetType::Next_Type);
	}
}

void UMenuChooseContent::CreateButtonsForContent()
{
	if (!ContentWidget)
	{
		return;
	}
	PanelForButtons->ClearChildren();

	TArray<UWidget*> Content = {};

	ContentWidget->GetContents(Content);

	if (Content.Num() > MaxButtons)
	{
		// Create new button:
		LeftSwapButton = CreateWidget<UButtonWidget>(this, SwapButtonClass);
		if (LeftSwapButton)
		{
			LeftSwapButton->SetKeyName("Left");
			FOnClickButton OnClickButton;
			OnClickButton.BindUFunction(this, "AnyButtonIsClick");
			LeftSwapButton->BindOnClick(OnClickButton);
			AddButtonInPanel(LeftSwapButton, -1);
		}

		RightSwapButton = CreateWidget<UButtonWidget>(this, SwapButtonClass);
		if (RightSwapButton)
		{
			RightSwapButton->SetKeyName("Right");
			FOnClickButton OnClickButton;
			OnClickButton.BindUFunction(this, "AnyButtonIsClick");
			RightSwapButton->BindOnClick(OnClickButton);
			AddButtonInPanel(RightSwapButton, -2);
		}

		return;
	}

	if (LeftSwapButton)
		LeftSwapButton->RemoveFromParent();
	if (RightSwapButton)
		RightSwapButton->RemoveFromParent();

	// if Content number < 1, not needed create button:
	if (Content.Num() > 1)
	{
		for (int32 i = 0; i < Content.Num(); i++)
		{
			// Create new button:
			auto NewButton = CreateWidget<UButtonWithTextWidget>(this, BaseButtonClass);

			if (NewButton)
			{
				// Set button key:
				NewButton->SetKeyName(*(FString::FromInt(i)));

				// Bind click event:
				FOnClickButton OnClickEvent;
				OnClickEvent.BindUFunction(this, "OnClickChooseButton");
				NewButton->BindOnClick(OnClickEvent);
				NewButton->SetText(FText::FromString(FString::FromInt(i + 1)));

				// Set new button in Panel:
				AddButtonInPanel(NewButton, i);
			}
		}
	}
}

void UMenuChooseContent::AddButtonInPanel_Implementation(UButtonWidget* NewButton, int32 IndexButton)
{
	if (PanelForButtons)
	{
		PanelForButtons->AddChild(NewButton);
	}
}

void UMenuChooseContent::OnClickChooseButton_Implementation(FName ButtonName)
{
	if (!ButtonName.IsNone() && ContentWidget)
	{
		// Take index:
		int32 NeededIndex = FCString::Atoi(*ButtonName.ToString());

		// Set info
		ContentWidget->SetInfoSheet(NeededIndex);
	}
}
