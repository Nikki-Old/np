// Fill out your copyright notice in the Description page of Project Settings.

#include "Frame/FrameWidget.h"
#include "Components/SizeBox.h"
#include "ITWidgets/Widget/Menu/MenuWidget.h"

bool UFrameWidget::Initialize()
{
	return Super::Initialize();
}

void UFrameWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (MainSizeBox)
	{
		MainSizeBox->WidthOverride = 1900.0f;
		MainSizeBox->HeightOverride = 1500.0f;
	}
}

void UFrameWidget::SetFrameSocket_Implementation(UMenuWidget* Widget)
{
	if (Widget)
	{
		const auto PrevMenuWidget = CurrentMenuWidget;
		CurrentMenuWidget = Widget;
		CurrentMenuWidget->Show(PrevMenuWidget);
		CurrentMenuWidget->SetFrameParent(this);
		OnNewFrameSocket.Broadcast(CurrentMenuWidget);
	}
}

UMenuWidget* UFrameWidget::GetFrameSocket_Implementation() const
{
	return CurrentMenuWidget;
}
