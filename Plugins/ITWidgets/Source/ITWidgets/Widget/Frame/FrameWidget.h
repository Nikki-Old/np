// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widget/WidgetBase.h"
#include "Interface/Frame.h"
#include "FrameWidget.generated.h"

/**
 *
 */

// Forward declaration:
class USizeBox;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNewFrameSocket, UMenuWidget*, NewMenuWidget);

UCLASS()
class ITWIDGETS_API UFrameWidget : public UWidgetBase, public IFrame
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FrameWidget", meta = (BindWidget))
	USizeBox* MainSizeBox = nullptr;

public:
	// Widget functions:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UPROPERTY(BlueprintAssignable, Category = "FrameWidget")
	FOnNewFrameSocket OnNewFrameSocket;

#pragma region IFrame
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IFrame")
	void SetFrameSocket(UMenuWidget* Widget);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "IFrame")
	UMenuWidget* GetFrameSocket() const;
#pragma endregion

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "FrameWidget")
	UMenuWidget* CurrentMenuWidget = nullptr;
};
