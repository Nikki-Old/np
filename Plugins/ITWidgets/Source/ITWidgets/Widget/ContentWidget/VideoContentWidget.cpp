// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/ContentWidget/VideoContentWidget.h"
#include "Widget/Video/VideoWidget.h"
#include "Components/WidgetSwitcher.h"
#include "MediaPlayer.h"
#include "MediaSource.h"

bool UVideoContentWidget::Initialize()
{
	return Super::Initialize();
}

void UVideoContentWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (ContentSwitcher)
	{
		CurrentVideoWidget = Cast<UVideoWidget>(ContentSwitcher->GetActiveWidget());

		if (CurrentVideoWidget)
		{
			if (MediaPlayer)
			{
				MediaPlayer->PlayOnOpen = false;
				MediaPlayer->OpenSource(CurrentVideoWidget->GetMediaSource());
			}
		}
	}
}

void UVideoContentWidget::PlayVideo()
{
	if (CurrentVideoWidget && MediaPlayer)
	{
		const auto TargetMediaSource = CurrentVideoWidget->GetMediaSource();
		// Stop re-open media source:
		if (!(MediaPlayer->GetUrl() == TargetMediaSource->GetUrl()))
		{
			MediaPlayer->OpenSource(TargetMediaSource);
		}

		MediaPlayer->Play();
	}
}

void UVideoContentWidget::SetPauseVideo(bool IsNewPause)
{
	if (MediaPlayer)
	{
		if (IsNewPause)
		{
			MediaPlayer->Pause();
		}
		else
		{
			PlayVideo();
		}

		bIsPaused = MediaPlayer->IsPaused();
	}
}

void UVideoContentWidget::StopVideo()
{
	if (MediaPlayer)
	{
		MediaPlayer->Reopen();
	}
}

void UVideoContentWidget::SetCurrentWidget(int Index)
{
	Super::SetCurrentWidget(Index);

	if (ContentSwitcher)
	{
		CurrentVideoWidget = Cast<UVideoWidget>(ContentSwitcher->GetActiveWidget());

		if (CurrentVideoWidget)
		{
			if (MediaPlayer)
			{
				MediaPlayer->OpenSource(CurrentVideoWidget->GetMediaSource());
			}
		}
	}
}
