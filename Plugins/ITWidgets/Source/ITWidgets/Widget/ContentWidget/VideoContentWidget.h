// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widget/ContentWidget/ContentWidgetBase.h"
#include "VideoContentWidget.generated.h"

/**
 *
 */

class UMediaPlayer;
class UVideoWidget;

UCLASS()
class ITWIDGETS_API UVideoContentWidget : public UContentWidgetBase
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "VideoContentWidget")
	UMediaPlayer* GetMediaPlayer() const { return MediaPlayer; }

	UFUNCTION()
	void PlayVideo();

	UFUNCTION()
	void SetPauseVideo(bool IsNewPause);

	UFUNCTION()
	void StopVideo();

protected:
	virtual void SetCurrentWidget(int Index) override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VideoContentWidget")
	UMediaPlayer* MediaPlayer = nullptr;

private:
	UPROPERTY()
	bool bIsPaused = false;

	UPROPERTY()
	UVideoWidget* CurrentVideoWidget = nullptr;
};
