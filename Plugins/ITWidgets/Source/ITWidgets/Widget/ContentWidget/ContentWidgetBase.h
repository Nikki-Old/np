// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ContentWidgetBase.generated.h"

/**
 *
 */

UENUM()
enum class ENextSheetType : uint8
{
	None_Type	  UMETA(DisplayName = "None"),
	Next_Type	  UMETA(DisplayName = "Next"),
	Previous_Type UMETA(DisplayName = "Previous")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFirstInfoSheet);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLastInfoSheet);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnMiddleInfoSheet);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeActiveWidget, int32, Index);

class UWidgetSwitcher;

UCLASS()
class ITWIDGETS_API UContentWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, Category = "ContentWidget")
	void SetInfoSheet(int Index, ENextSheetType NextSheetType = ENextSheetType::None_Type);

	UPROPERTY(BlueprintAssignable, Category = "ContentWidget")
	FOnFirstInfoSheet OnFirstInfoSheet;

	UPROPERTY(BlueprintAssignable, Category = "ContentWidget")
	FOnLastInfoSheet OnLastInfoSheet;

	UPROPERTY(BlueprintAssignable, Category = "ContentWidget")
	FOnMiddleInfoSheet OnMiddleInfoSheet;

	UPROPERTY(BlueprintAssignable, Category = "ContentWidget")
	FOnChangeActiveWidget OnChangeActiveWidget;

	int GetCurrentInfoIndex() const { return CurrentIndex; }
	int GetNumInfos() const { return NumInfoSheets; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ContentWidget")
	bool GetContents(TArray<UWidget*>& OutArray) const;

	UFUNCTION(BlueprintCallable, Category = "ContentWidget")
	void SetContents(TArray<UWidget*> NewContent);

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "ContentWidget", meta = (BindWidget))
	UWidgetSwitcher* ContentSwitcher = nullptr;

	/** Add contetn in ContentSwitcher: */
	UFUNCTION(BlueprintCallable, Category = "ContentWidget")
	int32 AddContent(UWidget* NewContent);

	UFUNCTION(BlueprintNativeEvent, Category = "ContentWidget")
	void NeededIndexMoreLength();
	void NeededIndexMoreLength_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "ContentWidget")
	void NeededIndexLessThanZero();
	void NeededIndexLessThanZero_Implementation();

	UFUNCTION(Category = "ContentWidget")
	virtual void SetCurrentWidget(int Index);

private:
	int NumInfoSheets = 0;

	int CurrentIndex = -1;
};
