// Fill out your copyright notice in the Description page of Project Settings.

#include "ContentWidgetBase.h"
#include "Blueprint/WidgetTree.h"
#include "Components/WidgetSwitcher.h"

bool UContentWidgetBase::Initialize()
{
	return Super::Initialize();
}

void UContentWidgetBase::NativeConstruct()
{
	Super::NativeConstruct();

	// FindInfoSheets();

	//// Collapsed all info sheet wisgets, but not first:
	// for (uint8 i = 1; i < InfoSheets.Num(); i++)
	//{
	//	if (InfoSheets.IsValidIndex(i))
	//	{
	//		InfoSheets[i]->SetVisibility(ESlateVisibility::Collapsed);
	//	}
	// }

	if (ContentSwitcher)
	{
		NumInfoSheets = ContentSwitcher->GetNumWidgets();
	}

	// Set Current info sheet to first:
	SetCurrentWidget(0);
}

void UContentWidgetBase::SetInfoSheet(int Index, ENextSheetType NextSheetType)
{
	switch (NextSheetType)
	{
		case ENextSheetType::None_Type:
		{
			auto NeededWidget = ContentSwitcher->GetWidgetAtIndex(Index);
			if (NeededWidget)
			{
				SetCurrentWidget(Index);
			}
			else if (NumInfoSheets <= Index)
			{
				NeededIndexMoreLength();
			}
			else
			{
				NeededIndexLessThanZero();
			}
			break;
		}
		case ENextSheetType::Next_Type:
			SetInfoSheet(CurrentIndex + 1);
			break;

		case ENextSheetType::Previous_Type:
			SetInfoSheet(CurrentIndex - 1);
			break;
	}
}

bool UContentWidgetBase::GetContents(TArray<UWidget*>& OutArray) const
{
	if (ContentSwitcher)
	{
		OutArray = ContentSwitcher->GetAllChildren();
		return true;
	}
	return false;
}

void UContentWidgetBase::SetContents(TArray<UWidget*> NewContent)
{
	if (ContentSwitcher)
	{
		ContentSwitcher->ClearChildren();
		for (auto Content : NewContent)
		{
			ContentSwitcher->AddChild(Content);
		}

		// Set Current info sheet to first:
		SetCurrentWidget(0);
		NumInfoSheets = ContentSwitcher->GetNumWidgets();
	}
}

int32 UContentWidgetBase::AddContent(UWidget* NewContent)
{
	if (ContentSwitcher)
	{
		ContentSwitcher->AddChild(NewContent);
		NumInfoSheets = ContentSwitcher->GetNumWidgets();
		return ContentSwitcher->GetChildIndex(NewContent);
	}

	return -1;
}

void UContentWidgetBase::NeededIndexMoreLength_Implementation()
{
	SetCurrentWidget(0);
}

void UContentWidgetBase::NeededIndexLessThanZero_Implementation()
{
	SetCurrentWidget(NumInfoSheets - 1);
}

void UContentWidgetBase::SetCurrentWidget(int Index)
{
	auto NeededWidget = ContentSwitcher->GetWidgetAtIndex(Index);
	if (NeededWidget)
	{
		// Save current index:
		CurrentIndex = Index;

		ContentSwitcher->SetActiveWidget(NeededWidget);

		if (CurrentIndex == 0)
		{
			OnFirstInfoSheet.Broadcast();
		}
		else if (CurrentIndex == NumInfoSheets - 1)
		{
			OnLastInfoSheet.Broadcast();
		}
		else
		{
			OnMiddleInfoSheet.Broadcast();
		}

		OnChangeActiveWidget.Broadcast(Index);
	}
}
