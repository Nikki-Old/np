// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HintWidget.generated.h"

/**
 *
 */

// Forward declaration:
class UTextBlock;

UCLASS()
class ITWIDGETS_API UHintWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HintWidget", meta = (BindWidget))
	UTextBlock* MainTextBlock = nullptr;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "HintWidget")
	bool SetTextByHintName(FName HintName);
	bool SetTextByHintName_Implementation(FName HintName);
};
