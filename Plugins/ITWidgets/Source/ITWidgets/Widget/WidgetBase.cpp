// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/WidgetBase.h"
#include "Animation/WidgetAnimation.h"

bool UWidgetBase::Initialize()
{
	return Super::Initialize();
}

void UWidgetBase::NativeConstruct()
{
	Super::NativeConstruct();

	OnReadyWidget.Broadcast(this);
}

bool UWidgetBase::ShowWidget_Implementation(UObject* NewParent)
{
	ParentWidget = NewParent;
	if (ShowAnimation)
	{
		PlayAnimation(ShowAnimation);
	}
	return true;
}

bool UWidgetBase::UnshowWidget_Implementation()
{
	if (UnShowAnimation)
	{
		PlayAnimation(UnShowAnimation);
		FWidgetAnimationDynamicEvent Delegate;
		Delegate.BindUFunction(this, FName("OnUnshowBroadcast"));
		BindToAnimationFinished(UnShowAnimation, Delegate);
	}
	else
	{
		OnUnshowBroadcast();
	}
	return true;
}

void UWidgetBase::ExecuteOnReadyWidget()
{
	OnReadyWidget.Broadcast(this);
}

void UWidgetBase::OnUnshowBroadcast()
{
	OnUnshowWidget.Broadcast();
}
