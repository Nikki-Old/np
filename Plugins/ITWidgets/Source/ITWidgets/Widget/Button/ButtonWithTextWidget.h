// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Widget/Button/ButtonWidget.h"
#include "Framework/Text/TextLayout.h"
#include "ButtonWithTextWidget.generated.h"

// Forward declaration:
class URichTextBlock;
class USizeBox;

/**
 *
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeText, FText, NewText);

UCLASS()
class ITWIDGETS_API UButtonWithTextWidget : public UButtonWidget
{
	GENERATED_BODY()

public:
	// Override default methods:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	// Main text block:
	UPROPERTY(BlueprintReadWrite, Category = "ButtonWithTextWidget", meta = (BindWidget))
	URichTextBlock* MainTextBlock = nullptr;

	UPROPERTY(BlueprintReadWrite, Category = "ButtonWithTextWidget", meta = (BindWidget))
	USizeBox* MainSizeBox = nullptr;

	/** Get text from button */
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "ButtonWithTextWidget")
	FText GetMainText() const;

	UFUNCTION(BlueprintCallable, Category = "ButtonWithTextWidget")
	void SetText(FText NewText);

	UPROPERTY(BlueprintAssignable, Category = "ButtonWithTextWidget")
	FOnChangeText OnChangeText;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ButtonWithTextWidget")
	void ApplySettings();
	void ApplySettings_Implementation();

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWithTextWidget | Text", meta = (MultiLine = true, ExposeOnSpawn = "true"))
	FText MainText = FText();

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWithTextWidget | Text")
	bool bIsAutoWrap = false;

	//UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWithTextWidget | Text")
	//float WrapTextAt = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWithTextWidget | Text")
	TEnumAsByte<ETextJustify::Type> Justify = ETextJustify::Center;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWithTextWidget | SizeBox")
	float Width = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWithTextWidget | SizeBox")
	float Height = 0.0f;
};
