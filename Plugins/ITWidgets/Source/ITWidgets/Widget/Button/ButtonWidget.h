// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ButtonWidget.generated.h"

/**
 *
 */

// Forward declaration:
class UButtonBase;

DECLARE_DYNAMIC_DELEGATE_OneParam(FOnClickButton, FName, ButtonName);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnHoveredButton, FName, ButtonName);
DECLARE_DYNAMIC_DELEGATE_OneParam(FOnUnhoveredButton, FName, ButtonName);

DECLARE_DELEGATE_OneParam(FOnChangeLockStatus, bool);

UCLASS()
class ITWIDGETS_API UButtonWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;

	virtual void NativeConstruct() override;

public:
	UFUNCTION(BlueprintCallable, Category = "ButtonWidget")
	void BindOnClick(FOnClickButton NewEvent);

	UFUNCTION(BlueprintCallable, Category = "ButtonWidget")
	void BindOnHovered(FOnHoveredButton NewEvent);

	UFUNCTION(BlueprintCallable, Category = "ButtonWidget")
	void BindOnUnhovered(FOnUnhoveredButton NewEvent);

	UFUNCTION(BlueprintCallable, Category = "ButtonWidget")
	void SetLockButton(bool IsLock);

	UFUNCTION(BLueprintCallable, BlueprintPure, Category = "ButtonWidget")
	FName GetKeyName() const { return ButtonKeyName; }

	/** Only for CPP */
	void SetKeyName(FName NewKeyName) { ButtonKeyName = NewKeyName; }

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWidget", meta = (ExposeOnSpawn = "true"))
	FName ButtonKeyName = FName();

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "ButtonWidget", meta = (ExposeOnSpawn = "true"))
	bool bIsLockButton = false;

	FOnChangeLockStatus OnChangeLockStatus;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ButtonWidget")
	void LockButton(bool Condition);
	void LockButton_Implementation(bool Condition);

#pragma region Delegate
	UPROPERTY()
	FOnClickButton OnClickButton;

	UPROPERTY()
	FOnHoveredButton OnHoveredButton;

	UPROPERTY()
	FOnUnhoveredButton OnUnhoveredButton;
#pragma endregion

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ButtonWidget", meta = (BindWidget))
	UButtonBase* MainButton = nullptr;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ButtonWidget")
	void ButtonIsClick();
	void ButtonIsClick_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ButtonWidget")
	void ButtonIsHovered();
	void ButtonIsHovered_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ButtonWidget")
	void ButtonIsUnhovered();
	void ButtonIsUnhovered_Implementation();
};
