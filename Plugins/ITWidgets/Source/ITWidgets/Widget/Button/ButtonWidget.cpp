// Fill out your copyright notice in the Description page of Project Settings.
#include "Widget/Button/ButtonWidget.h"
#include "Widget/Button/ButtonBase.h"

bool UButtonWidget::Initialize()
{
	return Super::Initialize();
}

void UButtonWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (MainButton)
	{
		if (!MainButton->OnClicked.IsAlreadyBound(this, &UButtonWidget::ButtonIsClick))
		{
			MainButton->OnClicked.AddDynamic(this, &UButtonWidget::ButtonIsClick);
			MainButton->OnHovered.AddDynamic(this, &UButtonWidget ::ButtonIsHovered);
			MainButton->OnUnhovered.AddDynamic(this, &UButtonWidget::ButtonIsUnhovered);
		}
	}

	OnChangeLockStatus.BindUFunction(this, "LockButton");
}

void UButtonWidget::BindOnClick(FOnClickButton NewEvent)
{
	if (!OnClickButton.IsBound())
	{
		OnClickButton = NewEvent;
	}
}

void UButtonWidget::BindOnHovered(FOnHoveredButton NewEvent)
{
	if (!OnHoveredButton.IsBound())
	{
		OnHoveredButton = NewEvent;
	}
}

void UButtonWidget::BindOnUnhovered(FOnUnhoveredButton NewEvent)
{
	if (!OnUnhoveredButton.IsBound())
	{
		OnUnhoveredButton = NewEvent;
	}
}

void UButtonWidget::SetLockButton(bool IsLock)
{
	bIsLockButton = IsLock;
	OnChangeLockStatus.ExecuteIfBound(bIsLockButton);
}

void UButtonWidget::LockButton_Implementation(bool Condition)
{
	// BP:
}

void UButtonWidget::ButtonIsClick_Implementation()
{
	if (!bIsLockButton)
	{
		OnClickButton.ExecuteIfBound(ButtonKeyName);
	}
}

void UButtonWidget::ButtonIsHovered_Implementation()
{
	if (!bIsLockButton)
	{
		OnHoveredButton.ExecuteIfBound(ButtonKeyName);
	}
}

void UButtonWidget::ButtonIsUnhovered_Implementation()
{
	if (!bIsLockButton)
	{
		OnUnhoveredButton.ExecuteIfBound(ButtonKeyName);
	}
}
