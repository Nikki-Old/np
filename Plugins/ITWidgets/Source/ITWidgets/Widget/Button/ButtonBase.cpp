// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Button/ButtonBase.h"
#include "ButtonBase.h"

#define LOCTEXT_NAMESPACE "UMG"

UButtonBase::UButtonBase()
{
	//// SlateWidgetStyleAsset'/ITWidgets/UI/Button/SW_Button_Base.SW_Button_Base'
	// static ConstructorHelpers::FObjectFinder<USlateWidgetStyleAsset> ButtonCustomStyle(TEXT("/ITWidgets/UI/Button/SW_Button_Base"));
	// if (ButtonCustomStyle.Succeeded())
	//{
	//	SButton::FArguments ButtonDefaults;

	//	ButtonDefaults.ButtonStyle(ButtonCustomStyle.Object);
	//	WidgetStyle = *ButtonDefaults._ButtonStyle;
	//}
}

#if (WITH_EDITOR)
const FText UButtonBase::GetPaletteCategory()
{
	return LOCTEXT("", "ITWidgets");
}
#endif

#undef LOCATEXT_NAMESPACE