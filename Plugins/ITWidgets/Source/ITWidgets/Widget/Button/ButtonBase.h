// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "ButtonBase.generated.h"

/**
 *
 */

UCLASS()
class ITWIDGETS_API UButtonBase : public UButton
{
	GENERATED_BODY()

public:
	UButtonBase();

#if WITH_EDITOR
	virtual const FText GetPaletteCategory() override;
#endif
};
