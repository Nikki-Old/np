// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Button/ButtonWithTextWidget.h"
#include "Components/RichTextBlock.h"
#include "Components/SizeBox.h"

bool UButtonWithTextWidget::Initialize()
{
	return Super::Initialize();
}

void UButtonWithTextWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

FText UButtonWithTextWidget::GetMainText() const
{
	if (MainTextBlock)
	{
		return MainTextBlock->GetText();
	}
	return FText();
}

void UButtonWithTextWidget::SetText(FText NewText)
{
	if (MainTextBlock)
	{
		MainText = NewText;
		MainTextBlock->SetText(MainText);
		OnChangeText.Broadcast(MainText);
	}
}

void UButtonWithTextWidget::ApplySettings_Implementation()
{
	if (MainTextBlock)
	{
		SetText(MainText);

		MainTextBlock->SetJustification(Justify);

		MainTextBlock->SetAutoWrapText(bIsAutoWrap);
		// MainTextBlock->WrapTextAt = WrapTextAt; CAN NOT
	}

	if (MainSizeBox)
	{
		MainSizeBox->SetWidthOverride(Width);
		MainSizeBox->SetHeightOverride(Height);
	}
}
