// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Interface/ShowWidgetInterface.h"
#include "WidgetBase.generated.h"

/**
 *
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnReadyWidget, UWidgetBase*, WidgetBase);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnUnshowWidget);

UCLASS()
class ITWIDGETS_API UWidgetBase : public UUserWidget, public IShowWidgetInterface
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UPROPERTY(BlueprintAssignable, Category = "WidgetBase")
	FOnReadyWidget OnReadyWidget;

	UPROPERTY(BlueprintAssignable, Category = "WidgetBase")
	FOnUnshowWidget OnUnshowWidget;

#pragma region IShowWidgetInterface

	/** Show widget. */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ShowWidgetInterface")
	bool ShowWidget(UObject* NewParent);
	virtual bool ShowWidget_Implementation(UObject* NewParent);

	/** Unshow widget. */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "ShowWidgetInterface")
	bool UnshowWidget();
	virtual bool UnshowWidget_Implementation();

#pragma endregion

protected:
	UFUNCTION(BlueprintCallable, Category = "WidgetBase")
	void ExecuteOnReadyWidget();

	UFUNCTION()
	void OnUnshowBroadcast();

	UPROPERTY(Transient, meta = (BindWidgetAnimOptional))
	UWidgetAnimation* ShowAnimation = nullptr;

	UPROPERTY(Transient, meta = (BindWidgetAnimOptional))
	UWidgetAnimation* UnShowAnimation = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "WidgetBase")
	UObject* ParentWidget = nullptr;

	// public:
	//	/** Get ParentWidget */
	//	UFUNCTION(BLueprintCallable, BlueprintPure, Category = "WidgetBase")
	//	UUserWidget* GetParentWidget() const { return ParentWidget; }
	//
	//	/** Set ParentWidget */
	//	UFUNCTION(BLueprintCallable, Category = "WidgetBase")
	//	void SetParentWidget(UUserWidget* NewParent) { ParentWidget = NewParent; }
	//
	// protected:
	//	/**  ParentWidget for this Widget */
	//	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ExposeOnSpawn = "true"))
	//	UUserWidget* ParentWidget = nullptr;
};
