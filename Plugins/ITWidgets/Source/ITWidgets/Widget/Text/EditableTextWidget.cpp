// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Text/EditableTextWidget.h"
#include "Components/TextBlock.h"

bool UEditableTextWidget::Initialize()
{
	return Super::Initialize();
}

void UEditableTextWidget::NativeConstruct()
{
	UpdateCarriage();

	Super::NativeConstruct();
}

void UEditableTextWidget::AddString(FString NewString)
{
	if (NewString.IsEmpty())
	{
		return;
	}

	auto CurrentString = GetArrayCharsText();

	CurrentString.InsertAt(CurrentCarriageIndex++, NewString);

	SetText(CurrentString);

	UpdateCarriage();
}

void UEditableTextWidget::RemoveChar()
{
	auto CurrentString = GetArrayCharsText();

	CurrentString.RemoveAt(CurrentCarriageIndex);
	if (CurrentString.IsValidIndex(CurrentCarriageIndex - 1))
	{
		CurrentString.RemoveAt(CurrentCarriageIndex - 1);
	}

	SetText(CurrentString);

	CurrentString.Append(Carriage);
	CurrentCarriageIndex = CurrentString.Len() - 1;

	SetText(CurrentString);
}

void UEditableTextWidget::MoveCarriage(bool IsForward)
{
	auto CurrentChars = GetArrayCharsText();

	if (IsForward)
	{
		if (CurrentChars.IsValidIndex(CurrentCarriageIndex + 1))
		{
			CurrentChars.RemoveAt(CurrentCarriageIndex);
			CurrentChars.InsertAt(++CurrentCarriageIndex, Carriage);
			SetText(CurrentChars);
		}
	}
	else
	{
		if (CurrentChars.IsValidIndex(CurrentCarriageIndex - 1))
		{
			CurrentChars.RemoveAt(CurrentCarriageIndex);
			CurrentChars.InsertAt(--CurrentCarriageIndex, Carriage);
			SetText(CurrentChars);
		}
	}
}

FString UEditableTextWidget::GetEditableText() const
{
	FString Text = FString();
	Text = MainTextBlock->GetText().ToString();
	Text.RemoveAt(CurrentCarriageIndex);
	return Text;
}

void UEditableTextWidget::UpdateCarriage()
{
	auto CurrentChars = GetArrayCharsText();
	CurrentChars.Append(Carriage);

	if (CurrentCarriageIndex > -1)
	{
		CurrentChars.RemoveAt(CurrentCarriageIndex);
	}

	CurrentCarriageIndex = CurrentChars.Len() - 1;

	SetText(CurrentChars);
}

void UEditableTextWidget::SetText(FString ArrayChars)
{
	FText NewText = FText::FromString(ArrayChars);
	MainTextBlock->SetText(NewText);
}

FString UEditableTextWidget::GetArrayCharsText() const
{
	auto CurrentText = GetMainText();
	auto CurrentString = CurrentText.ToString();

	return CurrentString;
}

FText UEditableTextWidget::GetMainText() const
{
	if (MainTextBlock)
	{

		return MainTextBlock->GetText();
	}

	return FText();
}
