// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HAL/Platform.h"
#include "EditableTextWidget.generated.h"

/**
 *
 */

class UTextBlock;

UCLASS()
class ITWIDGETS_API UEditableTextWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, Category = "EditableText")
	void AddString(FString NewString);

	UFUNCTION(BlueprintCallable, Category = "EditableText")
	void RemoveChar();

	UFUNCTION(BlueprintCallable, Category = "EditableText")
	void MoveCarriage(bool IsForward);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "EditableText")
	FString GetEditableText() const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "EditableText", meta = (BindWidget))
	UTextBlock* MainTextBlock = nullptr;

private:
	void UpdateCarriage();

	UFUNCTION()
	void SetText(FString ArrayChars);

	UFUNCTION()
	FString GetArrayCharsText() const;

	UFUNCTION()
	FText GetMainText() const;

	UPROPERTY()
	int32 CurrentCarriageIndex = -1;

	FString Carriage = "|";
};
