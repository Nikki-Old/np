// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "VideoWidget.generated.h"

/**
 *
 */

// Forward declaration:
class UImage;
class UMediaSource;

UCLASS()
class ITWIDGETS_API UVideoWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	virtual bool Initialize() override;

	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "VideoContentWidget")
	UMediaSource* GetMediaSource() const { return MediaSource; }

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "VideoWidget", meta = (BindWidget))
	UImage* VideoImage = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "VideoContentWidget")
	UMediaSource* MediaSource = nullptr;
};
