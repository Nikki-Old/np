// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Other/CountComboWidget.h"
#include "Components/RichTextBlock.h"
#include "Animation/WidgetAnimation.h"

bool UCountComboWidget::Initialize()
{
	return Super::Initialize();
}

void UCountComboWidget::NativeConstruct()
{
	Super::NativeConstruct();

	if (IncreaseAnim)
	{
		IncreaseIsEndEvent.BindUFunction(this, "SoftResetCounter");
		BindToAnimationFinished(IncreaseAnim, IncreaseIsEndEvent);
	}

	this->SetVisibility(ESlateVisibility::Collapsed);
}

void UCountComboWidget::ResetCounter_Implementation(bool bIsFail)
{
	if (CountCombo == 0)
	{
		return;
	}

	CountCombo = 0;
	UpdateCountComboText();

	if (bIsFail)
	{
		StopAllAnimations();
		PlayAnimation(FailCounterAnim);
	}
}

void UCountComboWidget::IncreaseCounter_Implementation()
{
	CountCombo++;
	UpdateCountComboText();

	if (this->GetVisibility() == ESlateVisibility::Collapsed)
	{
		this->SetVisibility(ESlateVisibility::HitTestInvisible);
	}

	if (IncreaseAnim)
	{
		UpdateColor();
		StopAllAnimations();
		OldCountCombo = CountCombo;
		PlayAnimation(IncreaseAnim);
	}
}

void UCountComboWidget::DecreaseCounter_Implementation()
{
	if (CountCombo <= 0)
	{
		return;
	}

	CountCombo--;

	UpdateCountComboText();

	if (DecreaseAnim)
	{
		UpdateColor();
		// UnbindFromAnimationFinished(DecreaseAnim, IncreaseIsEndEvent);
		StopAllAnimations();
		PlayAnimation(DecreaseAnim);
	}
}

void UCountComboWidget::UpdateCountComboText_Implementation()
{
	if (CountComboText)
	{
		CountComboText->SetText(FText::FromString(FString::FromInt(CountCombo)));
	}
}

void UCountComboWidget::UpdateColor_Implementation()
{
	// TO DO: create reasonable logic:
	FLinearColor NewColor = FLinearColor(FMath::RandRange(0.0f, 1.0f), FMath::RandRange(0.0f, 1.0f), FMath::RandRange(0.0f, 1.0f));

	this->SetColorAndOpacity(NewColor);
}

void UCountComboWidget::SoftResetCounter()
{
	if (OldCountCombo == CountCombo)
	{
		ResetCounter(false);
	}
}
