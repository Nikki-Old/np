// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "CountComboWidget.generated.h"

class URichTextBlock;
class UWidgetAnimation;

/**
 *
 */

UCLASS()
class ITWIDGETS_API UCountComboWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	// Widget functions:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	/** CountCombo = 0 */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "CountCombo")
	void ResetCounter(bool bIsFail = false);
	void ResetCounter_Implementation(bool bIsFail = false);

	/** CountCombo++ */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "CountCombo")
	void IncreaseCounter();
	void IncreaseCounter_Implementation();

	/** CountCombo-- */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "CountCombo")
	void DecreaseCounter();
	void DecreaseCounter_Implementation();

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Transient, Category = "CountCombo", meta = (BindWidgetAnim))
	UWidgetAnimation* FailCounterAnim = nullptr;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Transient, Category = "CountCombo", meta = (BindWidgetAnim))
	UWidgetAnimation* IncreaseAnim = nullptr;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Transient, Category = "CountCombo", meta = (BindWidgetAnim))
	UWidgetAnimation* DecreaseAnim = nullptr;

	/** Rich text block for inscription "Combo" */
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "CountCombo", meta = (BindWidget))
	URichTextBlock* ComboText = nullptr;

	/** Rich text block for inscription "Combo" */
	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "CountCombo", meta = (BindWidget))
	URichTextBlock* CountComboText = nullptr;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "CountCombo")
	int64 CountCombo = 0;

	UFUNCTION(BlueprintNativeEvent, Category = "CountCombo")
	void UpdateCountComboText();
	void UpdateCountComboText_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "CountCombo")
	void UpdateColor();
	void UpdateColor_Implementation();

private:
	UPROPERTY()
	FWidgetAnimationDynamicEvent IncreaseIsEndEvent;

	UFUNCTION()
	void SoftResetCounter();

	int64 OldCountCombo = 0;
};
