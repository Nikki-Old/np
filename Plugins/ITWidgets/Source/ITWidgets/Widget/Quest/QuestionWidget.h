// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/TextBlock.h"
#include "QuestionWidget.generated.h"

/**
 *
 */

// Forward declaration:
class UButtonWidget;
class UTextBlock;

/** Question type: */
UENUM(BlueprintType)
enum class EQuestionType : uint8
{
	None_Type UMETA(DisplayName = "None"),
	/** If have one answer, but one is right/true  */
	OneAnswer_Type UMETA(DisplayName = "OneAnswer"),
	/** If have lots of answer*/
	MultipleAnswers_Type UMETA(DisplayName = "MultipleAnswers"),
	/** Answer need write */
	WriteAnswer_Type UMETA(DisplayName = "WriteAnswer")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnConfirmAnswer, UQuestionWidget*, QuestionWidget);

UCLASS()
class ITWIDGETS_API UQuestionWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "QuestionWidget")
	void AddAnswer(FString NewAnswer);
	UFUNCTION(BlueprintCallable, Category = "QuestionWidget")
	void RemoveAnswer(FString Answer);

	UPROPERTY(BlueprintAssignable, Category = "QuestionWidget")
	FOnConfirmAnswer OnConfirmAnswer;

protected:
	virtual bool Initialize() override;
	virtual void NativeConstruct() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "QuestionWidget")
	void ClickAnswer(FName Answer);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "QuestionWidget")
	bool IsHaveAnswer(FString Answer) const;

	/** Question Name Text block: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "QuestionWidget", meta = (BindWidget))
	UTextBlock* QuestionNameTextBlock = nullptr;

	UFUNCTION(BlueprintCallable)
	void SetQuestionName(FName NewName);

	/** Text block for question: */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "QuestionWidget", meta = (BindWidget))
	UTextBlock* QuestionTextBlock = nullptr;

	/** Set text in QuestionTextBlock */
	UFUNCTION(BlueprintCallable, Category = "QuestionWidget")
	void SetQuestionText(FText QuestionText);

	/** Button for confirm the Answer */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "QuestionWidget", meta = (BindWidget))
	UButtonWidget* ConfirmButton = nullptr;

	/** Button for confirm answer, if choose/write any answer */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "QuestionWidget")
	void ConfirmAnswer(FName ButtonName);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "QuestionWidget", meta = (ExposeOnSpawn = "true"))
	EQuestionType QuestionType = EQuestionType::None_Type;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "QuestionWidget")
	TArray<FString> SelectedAnswer;

private:
	/** Flag for check - is have answer */
	bool bIsHaveAswer = false;
};
