// Fill out your copyright notice in the Description page of Project Settings.

#include "Widget/Quest/QuestionWidget.h"
#include "Widget/Button/ButtonWidget.h"

bool UQuestionWidget::Initialize()
{
	return Super::Initialize();
}

void UQuestionWidget::NativeConstruct()
{
	// Confirm button settings:
	if (ConfirmButton)
	{
		// Bind on click
		FOnClickButton OnClickButton;
		OnClickButton.BindUFunction(this, "ConfirmAnswer");
		ConfirmButton->BindOnClick(OnClickButton);
	}

	// Questiom text block settings:
	if (QuestionTextBlock)
	{
	}

	Super::NativeConstruct();
}

void UQuestionWidget::ClickAnswer_Implementation(FName Answer)
{
	if (!Answer.IsNone())
	{
		const auto AnswerStr = Answer.ToString();
		if (IsHaveAnswer(AnswerStr))
		{
			RemoveAnswer(AnswerStr);
		}
		else
		{
			AddAnswer(AnswerStr);
		}
	}
}

bool UQuestionWidget::IsHaveAnswer(FString Answer) const
{
	if (SelectedAnswer.Find(Answer) > -1)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void UQuestionWidget::SetQuestionName(FName NewName)
{
	if (QuestionNameTextBlock)
	{
		QuestionNameTextBlock->SetText(FText::FromString(NewName.ToString()));
	}
}

void UQuestionWidget::SetQuestionText(FText QuestionText)
{
	if (QuestionTextBlock)
	{
		QuestionTextBlock->SetText(QuestionText);
	}
}

void UQuestionWidget::ConfirmAnswer_Implementation(FName ButtonName)
{
	if (SelectedAnswer.Num() > 0)
	{
	}
	OnConfirmAnswer.Broadcast(this);
}

void UQuestionWidget::AddAnswer(FString NewAnswer)
{
	if (QuestionType == EQuestionType::MultipleAnswers_Type)
	{
		SelectedAnswer.AddUnique(NewAnswer);
	}
	else
	{
		if (SelectedAnswer.IsValidIndex(0))
		{
			SelectedAnswer[0] = NewAnswer;
		}
		else
		{
			SelectedAnswer.Add(NewAnswer);
		}
	}
}

void UQuestionWidget::RemoveAnswer(FString Answer)
{
	if (!Answer.IsEmpty())
	{
		const auto AnswerIndex = SelectedAnswer.Find(Answer);
		if (AnswerIndex > -1)
		{
			SelectedAnswer.RemoveAt(AnswerIndex);
		}
	}
}
