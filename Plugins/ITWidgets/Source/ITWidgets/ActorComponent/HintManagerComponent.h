// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Actor/Hint/HintBase.h"
#include "HintManagerComponent.generated.h"

class AHintBase;
// class EHintState;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChangeHintState, EHintState, NewState);

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ITWIDGETS_API UHintManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UHintManagerComponent();

	void AddHintInOwner(AHintBase* NewHint);

	UFUNCTION(BlueprintCallable, Category = "HintManager")
	void SetHintState(EHintState NewHintState);

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "HintManager")
	AHintBase* GetHintByKeyName(const FName KeyName) const;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintAssignable, Category = "HintManager")
	FOnChangeHintState OnChangeHintState;

	/** Current Hint State: */
	UPROPERTY(EditDefaultsOnly, Category = "HintManager")
	EHintState CurrentHintState = EHintState::None_State;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "HintManager")
	void SetShowHints();
	void SetShowHints_Implementation();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	/** Hint manager control this hints */
	UPROPERTY(VisibleAnyWhere, Category = "HintManager")
	TArray<AHintBase*> ControlledHints = {};
};
