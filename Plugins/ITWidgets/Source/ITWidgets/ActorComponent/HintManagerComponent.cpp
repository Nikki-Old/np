// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/HintManagerComponent.h"

// Sets default values for this component's properties
UHintManagerComponent::UHintManagerComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UHintManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

void UHintManagerComponent::AddHintInOwner(AHintBase* NewHint)
{
	if (NewHint)
	{
		if (ControlledHints.AddUnique(NewHint) > -1)
		{
			OnChangeHintState.AddDynamic(NewHint, &AHintBase::ChangeState);
			NewHint->ChangeState(CurrentHintState);
		}
	}
}

void UHintManagerComponent::SetHintState(EHintState NewHintState)
{
	if (CurrentHintState != NewHintState)
	{
		CurrentHintState = NewHintState;
		OnChangeHintState.Broadcast(CurrentHintState);
	}
}

AHintBase* UHintManagerComponent::GetHintByKeyName(const FName KeyName) const
{
	if (KeyName.IsNone())
	{
		return nullptr;
	}

	for (const auto Hint : ControlledHints)
	{

		if (Hint->GetHintKeyName() == KeyName)
		{
			return Hint;
		}
	}

	return nullptr;
}

void UHintManagerComponent::SetShowHints_Implementation()
{
}

// Called every frame
void UHintManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
