// Fill out your copyright notice in the Description page of Project Settings.

#include "UI/NPHUD.h"
#include "Blueprint/UserWidget.h"

#include "Interface/NPPlayerControllerInteface.h"

UUserWidget* ANPHUD::CreateMainMenuWidget_Implementation()
{
	return nullptr;
}

UUserWidget* ANPHUD::CreateHUDInGameWidget_Implementation()
{
	return nullptr;
}

UUserWidget* ANPHUD::CreatePauseWidget_Implementation()
{
	checkf(PauseWidgetClass, TEXT("PauseWidgetClass is not valid! Maybe not set in HUD."));

	if (!IsValid(PauseWidgetClass))
	{
		return nullptr;
	}

	// Save last widget:
	WidgetBeforePause = LastWidget;

	// Set collapsed last widget:
	WidgetBeforePause->SetVisibility(ESlateVisibility::Collapsed);

	// Create Pause widget:
	auto OwningPlayerController = GetOwningPlayerController();
	if (OwningPlayerController)
	{
		PauseWidget = CreateWidget(OwningPlayerController, PauseWidgetClass);
		PauseWidget->AddToViewport(1);

		if (PauseWidget)
		{

			if (OwningPlayerController->GetClass()->ImplementsInterface(UNPPlayerControllerInteface::StaticClass()))
			{
				// Save last input mode:
				LastInputType = INPPlayerControllerInteface::Execute_GetInputMode(OwningPlayerController);
				// Set UI mode:
				INPPlayerControllerInteface::Execute_SetInputModeByType(OwningPlayerController, EInputType::GameUI_Type, PauseWidget);
			}
		}
	}

	return PauseWidget;
}

void ANPHUD::RemovePauseWidget_Implementation()
{
	if (PauseWidget)
	{
		PauseWidget->RemoveFromParent();
		PauseWidget = nullptr;
	}

	auto OwningPlayerController = GetOwningPlayerController();
	if (OwningPlayerController)
	{
		if (OwningPlayerController->GetClass()->ImplementsInterface(UNPPlayerControllerInteface::StaticClass()))
		{
			// Set last input mode:
			INPPlayerControllerInteface::Execute_SetInputModeByType(OwningPlayerController, LastInputType, WidgetBeforePause);
			WidgetBeforePause->SetVisibility(ESlateVisibility::Visible);
		}
	}

	return;
}

void ANPHUD::SetLastWidget(UUserWidget* NewWidget)
{
	if (LastWidget)
	{
		LastWidget->RemoveFromParent();
	}

	LastWidget = NewWidget;
}
