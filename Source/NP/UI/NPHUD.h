// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "FunctionLibrary/NPTypes.h"
#include "NPHUD.generated.h"

/**
 *
 */

UCLASS()
class NP_API ANPHUD : public AHUD
{
	GENERATED_BODY()

public:
	/** Create Main Menu(Start Game, Settings, Exit and other) widget*/
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPHUD")
	UUserWidget* CreateMainMenuWidget();
	UUserWidget* CreateMainMenuWidget_Implementation();

	/** Create widget HUD in game: */
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPHUD")
	UUserWidget* CreateHUDInGameWidget();
	UUserWidget* CreateHUDInGameWidget_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPHUD")
	UUserWidget* CreatePauseWidget();
	UUserWidget* CreatePauseWidget_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPHUD")
	void RemovePauseWidget();
	void RemovePauseWidget_Implementation();

	/** Remove LastWidget and save new last widget: */
	UFUNCTION(BlueprintCallable, Category = "NPHUD")
	void SetLastWidget(UUserWidget* NewWidget);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "NPHUD")
	TSubclassOf<UUserWidget> PauseWidgetClass = nullptr;

	UPROPERTY(BlueprintReadOnly, Category = "NPHUD")
	UUserWidget* PauseWidget = nullptr;

private:
	UPROPERTY()
	UUserWidget* LastWidget = nullptr;

	UPROPERTY()
	UUserWidget* WidgetBeforePause = nullptr;

	UPROPERTY()
	EInputType LastInputType;
};
