// Copyright Epic Games, Inc. All Rights Reserved.

#include "NPGameModeBase.h"
#include "Kismet/KismetSystemLibrary.h"

#include "Controller/PlayerController/NPPlayerController.h"

APawn* ANPGameModeBase::GetPlayerPawnByController_Implementation(const ANPPlayerController* PlayerController)
{
	if (PlayerController && Players.Contains(PlayerController))
	{
		return Players[PlayerController];
	}

	return nullptr;
}

void ANPGameModeBase::SetPlayerPawn_Implementation(ANPPlayerController* PlayerController, APawn* NewPlayerPawn)
{
	if (PlayerController && NewPlayerPawn)
	{
		auto const Player = Players.Add(PlayerController, NewPlayerPawn);

		OnPlayerPawnIsRead.Broadcast(Player);
	}
}

void ANPGameModeBase::SetGamePause_Implementation(ANPPlayerController* PlayerController)
{
}

void ANPGameModeBase::ExitFromGame_Implementation()
{
	UKismetSystemLibrary::ExecuteConsoleCommand(GetWorld(), "Exit");
}

void ANPGameModeBase::SetInputModeForPlayer(APlayerController* TargetPlayerController, EInputType NewInputType, UWidget* WidgetFocus)
{
	if (!TargetPlayerController)
	{
		return;
	}

	for (auto Player : Players)
	{
		if (Player.Key == TargetPlayerController)
		{
			Player.Key->SetInputModeByType(NewInputType, WidgetFocus);
			return;
		}
	}
}

void ANPGameModeBase::StartGame_Implementation(const APlayerController* PlayerController)
{
}
