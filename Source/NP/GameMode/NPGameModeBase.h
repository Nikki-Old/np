// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Interface/GameModeInteract.h"
#include "NPGameModeBase.generated.h"

/**
 *
 */

class ANPPlayerController;
class UWidget;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerPawnIsRead, APawn*, PlayerPanw);

UCLASS()
class NP_API ANPGameModeBase : public AGameModeBase, public IGameModeInteract
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "GameModeBase")
	FOnPlayerPawnIsRead OnPlayerPawnIsRead;

#pragma region IGameModeInteract

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeInteract")
	APawn* GetPlayerPawnByController(const ANPPlayerController* PlayerController);
	APawn* GetPlayerPawnByController_Implementation(const ANPPlayerController* PlayerController);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeInteract")
	void SetPlayerPawn(ANPPlayerController* PlayerController, APawn* NewPlayerpawn);
	void SetPlayerPawn_Implementation(ANPPlayerController* PlayerController, APawn* NewPlayerpawn);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeInteract")
	void SetGamePause(ANPPlayerController* PlayerController);
	void SetGamePause_Implementation(ANPPlayerController* PlayerController);

#pragma endregion

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeBase")
	void ExitFromGame();
	void ExitFromGame_Implementation();

protected:
	UFUNCTION(BlueprintCallable, Category = "GameModeBase")
	void SetInputModeForPlayer(APlayerController* TargetPlayerController, EInputType NewInputType, UWidget* WidgetFocus = nullptr);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeBase")
	void StartGame(const APlayerController* PlayerController);
	void StartGame_Implementation(const APlayerController* PlayerController);

	UPROPERTY(BlueprintReadOnly, Category = "GameModeBase")
	TMap<ANPPlayerController*, APawn*> Players = {};
};
