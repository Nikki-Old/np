// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Animation/Notify/DealDamageAN.h"
#include "FunctionLibrary/NPTypes.h"
#include "ComboAttackComponent.generated.h"

class USkeletalMeshComponent;

USTRUCT(BlueprintType)
struct FComboPartInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ComboPart")
	EAttackKey AttackKey = EAttackKey::None_Key;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ComboPart")
	UAnimMontage* Animation = nullptr;

	/** Speed playing animation */
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ComboPart")
	float ScaleRate = 1.0f;
};

USTRUCT(BlueprintType)
struct FComboInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "ComboInformation")
	TArray<FComboPartInfo> Combo;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStartAttack, UAnimMontage*, AnimMontage, int32, ComboNumber);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnEndAttack);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnCanDealDamage, FDealDamageInfo, DealDamageInfo);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class NP_API UComboAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UComboAttackComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	UPROPERTY(BlueprintAssignable, Category = "AttackCombo")
	FOnStartAttack OnStartAttack;
	UPROPERTY(BlueprintAssignable, Category = "AttackCombo")
	FOnEndAttack OnEndAttack;

	UPROPERTY(BlueprintAssignable, Category = "AttackCombo")
	FOnCanDealDamage OnCanDealDamage;

	UFUNCTION(BlueprintCallable, Category = "AttackCombo")
	bool Attack(EAttackKey AttackKey, bool bIsForce = false);

	UFUNCTION(BlueprintCallable, Category = "AttackCombo")
	void StopAttack();

protected:
	/** All combos inforamation */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AttackCombo")
	TArray<FComboInfo> ComboInformation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "AttackCombo")
	float StopBlendOut = 0.5f;

	UFUNCTION(BlueprintPure, Category = "AttackCombo")
	bool GetAnimMontageForCombo(EAttackKey AttackKey, int CurrentComboNumber, FComboPartInfo& OutInformation);

	UFUNCTION()
	void StartEndCurrentAnim(USkeletalMeshComponent* SkeletalMesh);
	UFUNCTION()
	void DealDamageCurrentAnim(FDealDamageInfo DealDamageInfo);

	UFUNCTION()
	void EndCurrentAnim(UAnimMontage* Montage, bool bInterrupted);

private:
	TArray<FComboInfo> AvailableComboInformation = {};

	EAttackKey LastAttackKey = EAttackKey::None_Key;
	bool bIsCanAddComboNumber = true;
	bool bIsStartAttack = false;

	int32 ComboNumber = 0;
	int32 OldComboNumber = 0;

	UPROPERTY()
	UAnimInstance* CurrentAnimInstance = nullptr;

	UPROPERTY()
	UAnimMontage* CurrentAnimMontage = nullptr;
	UPROPERTY()
	USkeletalMeshComponent* OwnerMesh = nullptr;
};
