// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "NRCharacterMovementComponent.generated.h"

/**
 *
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAddInputVector);

UCLASS()
class NP_API UNRCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()

public:
	virtual bool IsFalling() const override;

	virtual void AddInputVector(FVector WorldVector, bool bForce = false) override;

	UPROPERTY(BlueprintAssignable, Category = "Character Movemenet")
	FOnAddInputVector OnAddInputVector;

protected:
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

#pragma region Jump
protected:
	/** Override base function: */
	virtual bool DoJump(bool bReplayingMoves) override;

	/** Jump curve */
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Character Movement: Jumping / Falling")
	UCurveFloat* JumpCurve = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "Character Movement: Jumping / Falling")
	UCurveFloat* FallCurve = nullptr;

private:
	bool bIsJumping = false;
	float JumpTime;
	float PrevJumpCurveValue = -1.0f;
	float JumpMinTime = -1.0f;
	float JumpMaxTime = -1.0f;
	void ProccessCustomJumping(float DeltaTime);

	// Falling:
	bool bIsFalling = false;
	float FallTime;
	float PrevFallCurveValue = -1.0f;
	float FallMinTime = -1.0f;
	float FallMaxTime = -1.0f;

	void SetCustomFallingMode();
	void ProccessCustomFalling(float DeltaTime);

	bool CustomFindFloor(FFindFloorResult& OutFloorResult, const FVector Start, const FVector End);

#pragma endregion
};
