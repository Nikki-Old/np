// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "DashComponent.generated.h"

class UCameraComponent;
class USphereComponent;

USTRUCT(BlueprintType)
struct FDashInformation
{
	GENERATED_BODY()

	FDashInformation()
		: TargetActor(nullptr)
		, BodyMesh(nullptr)
		, NearestSocketPosition(FName()) {}

	FDashInformation& operator=(const FDashInformation& Other)
	{
		TargetActor = Other.TargetActor;
		BodyMesh = Other.BodyMesh;
		NearestSocketPosition = Other.NearestSocketPosition;
		return *this;
	}

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DashInformation")
	AActor* TargetActor = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DashInformation")
	USceneComponent* BodyMesh = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DashInformation")
	FName NearestSocketPosition = FName();
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSetTarget, AActor*, Target);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStartDash, FDashInformation, DashInformation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEndDash, FDashInformation, DashInformation);

UCLASS(Blueprintable, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class NP_API UDashComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UDashComponent();

	UFUNCTION(BlueprintCallable, Category = "DashComponent")
	bool DashToDirection(const FVector Direction);

	UFUNCTION(BlueprintCallable, Category = "DashComponent")
	void Dash();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DashComponent")
	bool bShowDebug = true;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DashComponent")
	float MaxDistanceDash = 250.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DashComponent", meta = (ClampMin = "0.001", UMin = "0.001"))
	float HowOftenUpdateDashMove = 0.01f;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "DashComponent")
	UCurveFloat* DashCurve = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DashComponent", meta = (ClampMin = "0.001", UMin = "0.001"))
	float AcceptDistance = 25.0f;

	UPROPERTY(BlueprintAssignable, Category = "DashComponent")
	FOnSetTarget OnSetTarget;
	UPROPERTY(BlueprintAssignable, Category = "DashComponent")
	FOnStartDash OnStartDash;
	UPROPERTY(BlueprintAssignable, Category = "DashComponent")
	FOnEndDash OnEndDash;

	UFUNCTION(BlueprintCallable, Category = "DashComponent")
	void StartDash(const FVector TargetLocation);
	UFUNCTION(BlueprintCallable, Category = "DashComponent")
	void EndDashMove();

	UFUNCTION(BlueprintCallable, Category = "DashComponent")
	void SetFindTargetForDash(bool bIsFind) { bIsFindTarget = bIsFind; }

	UFUNCTION(BlueprintCallable, Category = "DashComponent")
	void DashToCurrentTarget();

	UFUNCTION(BlueprintCallable, Category = "DashComponent")
	void SetTargetDash(AActor* NewTarget);

	UFUNCTION(BlueprintPure, Category = "DashComponent")
	void GetDashInformation(FDashInformation& OutInfo) const { OutInfo = DashInfo; }

protected:
	void FindTargetCollision();

	UPROPERTY()
	USphereComponent* TargetCollision = nullptr;

	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "DashComponent")
	float LineTraceDistance = 10000.0f;

	UFUNCTION()
	bool FindNearestSocketPosition(AActor* Actor, FName& NearestSocket);

	UFUNCTION()
	void UpdateDashMove();

	UFUNCTION()
	void FindTargetActorForDash();

	UFUNCTION(BlueprintNativeEvent, Category = "DashComponent")
	void SetTargetDashFX(AActor* TargetDash);
	void SetTargetDashFX_Implementation(AActor* TargetDash);

	UFUNCTION(BlueprintNativeEvent, Category = "DashComponent")
	void TargetCollisionBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void TargetCollisionBeginOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintNativeEvent, Category = "DashComponent")
	void TargetCollisionEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	void TargetCollisionEndOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	bool FindTargetInOverlapTargets();

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
	UFUNCTION()
	void FindOverlapObjects();

	UPROPERTY()
	UCameraComponent* OwnerCamera = nullptr;
	bool bIsFindTarget = false;

	UPROPERTY()
	FDashInformation DashInfo;

	FTimerHandle DashMoveTimer;

	FVector TargetDashLocation = FVector(0);
	FVector CurrentOwnerLocation = FVector(0);
	float DashTime = 0.0f;

	UPROPERTY()
	TArray<AActor*> OverlapTargets = {};

	bool bIsDash = false;
};
