// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/Movement/NRCharacterMovementComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/CapsuleComponent.h"

void UNRCharacterMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	if (JumpCurve)
	{
		// Save curve time range:
		JumpCurve->GetTimeRange(JumpMinTime, JumpMaxTime);
	}

	if (FallCurve)
	{
		FallCurve->GetTimeRange(FallMinTime, FallMaxTime);
	}
}

void UNRCharacterMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	ProccessCustomJumping(DeltaTime);
	ProccessCustomFalling(DeltaTime);
}

bool UNRCharacterMovementComponent::DoJump(bool bReplayingMoves)
{
	if (CharacterOwner && CharacterOwner->CanJump())
	{
		if (!bConstrainToPlane || FMath::Abs(PlaneConstraintNormal.Z) != 1.0f)
		{
			if (JumpCurve)
			{
				if (!bIsJumping)
				{
					SetMovementMode(EMovementMode::MOVE_Flying);

					bIsJumping = true;
					bIsFalling = false;
					JumpTime = JumpMinTime;
					PrevJumpCurveValue = JumpCurve->GetFloatValue(JumpTime);

					// CAN TO DO: start jump sound

					return true;
				}
			}
			else
			{
				return Super::DoJump(bReplayingMoves);
			}
		}
	}

	return false;
}

void UNRCharacterMovementComponent::ProccessCustomJumping(float DeltaTime)
{
	// If start jump and have JumpCurve...
	if (bIsJumping && JumpCurve)
	{
		// Increase Jump time:
		JumpTime += DeltaTime;

		if (JumpTime <= JumpMaxTime)
		{
			float JumpCurveValue = JumpCurve->GetFloatValue(JumpTime);
			float JumpCurveValueDelta = JumpCurveValue - PrevJumpCurveValue;
			PrevJumpCurveValue = JumpCurveValue;

			Velocity.Z = 0.0f;
			float CustomVelocity = JumpCurveValueDelta / DeltaTime;

			FVector ActorLocation = GetActorLocation();
			FVector DestinationLocation = ActorLocation + FVector(0.0f, 0.0f, JumpCurveValueDelta);

			// Check for roof collisions if Chracter is moving up:
			if (CustomVelocity > 0.0f)
			{
				FCollisionQueryParams RoofCheckCollisionParams;
				RoofCheckCollisionParams.AddIgnoredActor(CharacterOwner);
				FCollisionShape CapsuleShape = FCollisionShape::MakeCapsule(CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleRadius(), CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());

				FHitResult RoofHitResult;
				bool bIsBlockingHit = GetWorld()->SweepSingleByProfile(RoofHitResult, ActorLocation, DestinationLocation, CharacterOwner->GetActorRotation().Quaternion(), CharacterOwner->GetCapsuleComponent()->GetCollisionProfileName(), CapsuleShape, RoofCheckCollisionParams);

				if (bIsBlockingHit)
				{
					// Roof collision hit:
					bIsJumping = false;
					CharacterOwner->ResetJumpState();

					Velocity.Z = 0.0f;

					// Take Character to safe location where its not hitting roof.
					// This can be improved by using the impact location and using that.
					DestinationLocation = ActorLocation;

					SetCustomFallingMode();
				}
			}

			// What is it?
			FLatentActionInfo LatentInfo;
			LatentInfo.CallbackTarget = this;
			UKismetSystemLibrary::MoveComponentTo((USceneComponent*)CharacterOwner->GetCapsuleComponent(), DestinationLocation, CharacterOwner->GetActorRotation(), false, false, 0.0f, true, EMoveComponentAction::Type::Move, LatentInfo);

			if (CustomVelocity < 0.0f)
			{
				// Falling check floor:
				const FVector CapsuleLocation = UpdatedComponent->GetComponentLocation();
				FFindFloorResult FloorResult;
				bool bIsFloor = CustomFindFloor(FloorResult, CapsuleLocation, DestinationLocation);
				const auto FloorDistance = FloorResult.GetDistanceToFloor();
				if (bIsFloor)
				{
					if (FloorDistance < FMath::Abs(JumpCurveValueDelta))
					{
						DestinationLocation = CapsuleLocation + FVector(0.0f, 0.0f, JumpCurveValueDelta);
					}
					SetMovementMode(EMovementMode::MOVE_Walking);

					bIsJumping = false;
					CharacterOwner->ResetJumpState();
				}
			}
		}
		else
		{
			const FVector CapsuleLocation = UpdatedComponent->GetComponentLocation();
			FFindFloorResult FloorResult;
			FindFloor(CapsuleLocation, FloorResult, false);

			if (FloorResult.IsWalkableFloor() && IsValidLandingSpot(CapsuleLocation, FloorResult.HitResult))
			{
				SetMovementMode(EMovementMode::MOVE_Walking);
			}
			else
			{
				SetCustomFallingMode();
			}

			bIsJumping = false;
			CharacterOwner->ResetJumpState();
		}
	}
}

void UNRCharacterMovementComponent::SetCustomFallingMode()
{
	if (FallCurve)
	{
		bIsFalling = true;
		FallTime = 0.0f;
		PrevFallCurveValue = 0.0f;
		Velocity.Z = 0.0f;

		SetMovementMode(EMovementMode::MOVE_Flying);
	}
	else
	{
		SetMovementMode(EMovementMode::MOVE_Falling);
	}
}

void UNRCharacterMovementComponent::ProccessCustomFalling(float DeltaTime)
{
	if (FallCurve)
	{
		if (bIsFalling)
		{
			FallTime += DeltaTime;

			float FallCurveValue = FallCurve->GetFloatValue(FallTime);
			float DeltaFallCurveValue = FallCurveValue - PrevFallCurveValue;
			PrevFallCurveValue = FallCurveValue;

			// Setting velocity:
			// Velocity.Z = DeltaFallCurveValue / DeltaTime;
			Velocity.Z = 0.0f;

			FVector CapsuleLocation = UpdatedComponent->GetComponentLocation();
			FVector DestinationLocation = CapsuleLocation + FVector(0.0f, 0.0f, DeltaFallCurveValue);

			FFindFloorResult FloorResult;
			const bool bIsFloor = CustomFindFloor(FloorResult, CapsuleLocation, DestinationLocation);
			if (bIsFloor)
			{
				const float FloorDistance = FloorResult.GetDistanceToFloor();

				if (FloorDistance < FMath::Abs(DeltaFallCurveValue))
				{
					// Since going down, substract the floor distance:
					DestinationLocation = CapsuleLocation + FVector(0.0f, 0.0f, -FloorDistance);

					bIsFalling = false;

					// Stopping the chracter and cancelling all the movementum carried from before tha jump/fall
					// Remove if youy want to carry the movement
					Velocity = FVector::ZeroVector;

					SetMovementMode(EMovementMode::MOVE_Walking);
				}
			}

			FLatentActionInfo LatentInfo;
			LatentInfo.CallbackTarget = this;
			UKismetSystemLibrary::MoveComponentTo((USceneComponent*)CharacterOwner->GetCapsuleComponent(), DestinationLocation, CharacterOwner->GetActorRotation(), false, false, 0.0f, true, EMoveComponentAction::Type::Move, LatentInfo);
		}
		else if (MovementMode == EMovementMode::MOVE_Falling)
		{
			// Dropping down from edge wheli walking
			// Note that this will effect default jump since default jump uses falling mode
			SetCustomFallingMode();
		}
	}
}

bool UNRCharacterMovementComponent::CustomFindFloor(FFindFloorResult& OutFloorResult, const FVector Start, const FVector End)
{
	FCollisionQueryParams RoofCheckCollisionParams;
	RoofCheckCollisionParams.AddIgnoredActor(CharacterOwner);
	FCollisionShape CapsuleShape = FCollisionShape::MakeCapsule(CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleRadius(), CharacterOwner->GetCapsuleComponent()->GetScaledCapsuleHalfHeight());

	FHitResult FloorHitResult;
	bool bIsBlockingHit = GetWorld()->SweepSingleByProfile(FloorHitResult, Start, End, CharacterOwner->GetActorRotation().Quaternion(), CharacterOwner->GetCapsuleComponent()->GetCollisionProfileName(), CapsuleShape, RoofCheckCollisionParams);

	if (bIsBlockingHit)
	{
		FindFloor(Start, OutFloorResult, false, &FloorHitResult);

		// if valid floor or if the amount needed to move in this frame is lot more than distance to floor
		// consider that floor is found and set character to floor location/
		return (OutFloorResult.IsWalkableFloor() && IsValidLandingSpot(Start, OutFloorResult.HitResult));
	}

	return false;
}

bool UNRCharacterMovementComponent::IsFalling() const
{
	if (JumpCurve)
	{
		return Super::IsFalling() || bIsJumping || bIsFalling;
	}
	else
	{
		return Super::IsFalling();
	}
}

void UNRCharacterMovementComponent::AddInputVector(FVector WorldVector, bool bForce)
{
	Super::AddInputVector(WorldVector, bForce);

	OnAddInputVector.Broadcast();
}
