// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/Movement/DashComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/MeshComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"

// Sets default values for this component's properties
UDashComponent::UDashComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UDashComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	FindTargetCollision();
}

void UDashComponent::FindTargetCollision()
{
	if (GetOwner())
	{
		const auto SphereComponents = GetOwner()->GetComponentsByTag(USphereComponent::StaticClass(), "TargetCollision");
		check(SphereComponents.IsValidIndex(0));

		TargetCollision = Cast<USphereComponent>(SphereComponents[0]);
		if (TargetCollision)
		{
			TargetCollision->SetCollisionProfileName("FinderDashTarget", true);
			TargetCollision->OnComponentBeginOverlap.AddDynamic(this, &UDashComponent::TargetCollisionBeginOverlap);
			TargetCollision->OnComponentEndOverlap.AddDynamic(this, &UDashComponent::TargetCollisionEndOverlap);

			FindOverlapObjects();
		}
	}
}

void UDashComponent::FindOverlapObjects()
{
	if (TargetCollision)
	{
		TArray<AActor*> OverlapActors = {};
		TargetCollision->GetOverlappingActors(OverlapActors);

		for (const auto Actor : OverlapActors)
		{
			OverlapTargets.Add(Actor);
		}
	}
}

void UDashComponent::Dash()
{
	if (!DashMoveTimer.IsValid())
	{
		if (DashInfo.TargetActor)
		{
			DashToCurrentTarget();
		}
		else
		{
			if (FindTargetInOverlapTargets())
			{
				DashToCurrentTarget();
			}
			else if (OwnerCamera)
			{
				// is Bad:
				// DashToDirection(OwnerCamera->GetForwardVector());

				DashToDirection(GetOwner()->GetActorForwardVector());
			}
		}
	}
}

bool UDashComponent::DashToDirection(const FVector Direction)
{
	if (!GetWorld() || !GetOwner())
	{
		return false;
	}

	const auto StartLocation = GetOwner()->GetActorLocation();
	const auto EndLocation = StartLocation + (Direction * MaxDistanceDash);

	const TArray<AActor*> IgnoreActors = { GetOwner() };
	FHitResult HitResult;

	UKismetSystemLibrary::LineTraceSingle(GetWorld(), StartLocation, EndLocation, ETraceTypeQuery::TraceTypeQuery1, false, IgnoreActors, EDrawDebugTrace::ForDuration, HitResult, true, FLinearColor::Black, FLinearColor::White);

	FVector NewTargetLocation = FVector(0);
	if (HitResult.bBlockingHit)
	{
		NewTargetLocation = HitResult.Location;
	}
	else
	{
		NewTargetLocation = EndLocation;
	}
	StartDash(NewTargetLocation);

	return true;
}

void UDashComponent::DashToCurrentTarget()
{
	FVector NewTargetLocation = FVector(0);

	if (FindNearestSocketPosition(
			DashInfo.TargetActor, DashInfo.NearestSocketPosition))
	{
		NewTargetLocation = DashInfo.BodyMesh->GetSocketLocation(DashInfo.NearestSocketPosition);
	}
	else
	{
		NewTargetLocation = DashInfo.BodyMesh->GetComponentLocation();
	}

	StartDash(NewTargetLocation);
}

void UDashComponent::SetTargetDash(AActor* NewTarget)
{
	if (NewTarget)
	{
		DashInfo.TargetActor = NewTarget;
		SetTargetDashFX(DashInfo.TargetActor);
	}
	else
	{
		DashInfo = FDashInformation();
	}

	OnSetTarget.Broadcast(DashInfo.TargetActor);
}

void UDashComponent::SetTargetDashFX_Implementation(AActor* TargetDash)
{
	if (TargetDash)
	{
		// GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Green, TargetDash->GetName());
	}
}

bool UDashComponent::FindNearestSocketPosition(AActor* Actor, FName& NearestSocket)
{
	if (Actor)
	{
		// Find Mesh component with tag "Body":
		const auto MeshsWithTag = Actor->GetComponentsByTag(UMeshComponent::StaticClass(), "Body");
		if (MeshsWithTag.IsValidIndex(0))
		{
			const auto BodyMesh = Cast<USceneComponent>(MeshsWithTag[0]);
			if (BodyMesh)
			{
				// Get All Sockets:
				const auto AllSockets = BodyMesh->GetAllSocketNames();

				// Find sockets with "Position" substring:
				TArray<FName> PositionSockets = {};
				for (const auto SocketName : AllSockets)
				{
					if (UKismetStringLibrary::FindSubstring(SocketName.ToString(), "Position") > -1)
					{
						PositionSockets.Add(SocketName);
					}
				}

				// Find the nearest socket:
				const auto OwnerLocation = GetOwner()->GetActorLocation();
				float MinDistance = 100000000.0f;
				for (const auto PositionSocketName : PositionSockets)
				{
					const auto CurrentSocketLocation = BodyMesh->GetSocketLocation(PositionSocketName);
					const auto CurrentDistance = UKismetMathLibrary::Vector_Distance(GetOwner()->GetActorLocation(), CurrentSocketLocation);
					if (CurrentDistance < MinDistance)
					{
						MinDistance = CurrentDistance;
						NearestSocket = PositionSocketName;
					}
				}
				bool bIsFind = MinDistance != 100000000.0f;
				if (bIsFind)
				{
					DashInfo.BodyMesh = BodyMesh;
				}

				return bIsFind;
			}
		}
	}
	return false;
}

void UDashComponent::StartDash(const FVector TargetLocation)
{
	if (!DashMoveTimer.IsValid())
	{
		//	// TO DO: move this code in Player character or owner decides for himself - what to do:
		//	// Set rotation Owner to Target actor:
		//	auto Direction = (TargetActor->GetActorLocation() - GetOwner()->GetActorLocation()).GetSafeNormal();
		//	GetOwner()->SetActorRotation(Direction.Rotation());
		//	GetOwner()->SetActorEnableCollision(false);
		//	OnStartDash.Broadcast();
		//}

		DashTime = 0;
		TargetDashLocation = TargetLocation;
		CurrentOwnerLocation = GetOwner()->GetActorLocation();

		if (bShowDebug)
		{
			UKismetSystemLibrary::DrawDebugSphere(GetWorld(), TargetDashLocation, 10.0f, 12, FLinearColor::Red, 5.0f, 2.0f);
		}

		// Start update Owner movement:
		GetWorld()->GetTimerManager().SetTimer(DashMoveTimer, this, &UDashComponent::UpdateDashMove, HowOftenUpdateDashMove, true);
		bIsDash = true;
		OnStartDash.Broadcast(DashInfo);
	}
}
void UDashComponent::UpdateDashMove()
{

	DashTime += GetWorld()->GetDeltaSeconds();

	const auto Distance = UKismetMathLibrary::Vector_Distance(GetOwner()->GetActorLocation(), TargetDashLocation);

	if (Distance > AcceptDistance)
	{
		// TargetDashLocation.Z = CurrentOwnerLocation.Z;

		float Alpha = 0.0f;
		if (DashCurve)
		{
			Alpha = DashCurve->GetFloatValue(DashTime);
		}
		else
		{
			Alpha = DashTime;
		}
		GetOwner()->SetActorLocation(FMath::Lerp(CurrentOwnerLocation, TargetDashLocation, Alpha), true);
	}
	else
	{
		EndDashMove();
	}
}

void UDashComponent::EndDashMove()
{
	if (DashMoveTimer.IsValid())
	{
		GetWorld()->GetTimerManager().ClearTimer(DashMoveTimer);
	}

	OnEndDash.Broadcast(DashInfo);
	bIsDash = false;
}

// Called every frame
void UDashComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bIsFindTarget && !bIsDash)
	{
		FindTargetActorForDash();
	}

	if (bShowDebug)
	{
		FString DebugMessage = FString::Printf(TEXT("CurrentTarget: %s"), *DashInfo.TargetActor->GetName());
		GEngine->AddOnScreenDebugMessage(1, 1.0f, FColor::Red, DebugMessage);
	}
}

void UDashComponent::FindTargetActorForDash()
{
	if (!OwnerCamera)
	{
		const auto OwnerCameras = GetOwner()->GetComponentsByTag(UCameraComponent::StaticClass(), "MainCamera");
		if (OwnerCameras.IsValidIndex(0))
		{
			OwnerCamera = Cast<UCameraComponent>(OwnerCameras[0]);
		}
	}

	if (OwnerCamera)
	{
		bool bIsFind = false;

		const auto StartLocation = GetOwner()->GetActorLocation();
		auto CameraDir = (OwnerCamera->GetForwardVector() * LineTraceDistance);
		CameraDir.Z = 0.0f;
		const auto EndLocation = StartLocation + CameraDir;

		TArray<AActor*> IgnoreActors = { GetOwner() };
		FHitResult HitResult;

		if (UKismetSystemLibrary::LineTraceSingle(GetWorld(), StartLocation, EndLocation, //
				ETraceTypeQuery::TraceTypeQuery1, false,								  //
				IgnoreActors, EDrawDebugTrace::ForOneFrame, HitResult, true))			  //
		{
			if (HitResult.Actor.Get())
			{
				if (HitResult.Actor != DashInfo.TargetActor)
				{
					if (HitResult.Actor->ActorHasTag("TargetDash"))
					{
						SetTargetDash(HitResult.Actor.Get());
						bIsFind = true;
						
					}
				}
				else
				{
					bIsFind = true;
				}
			}
		}

		//if (!bIsFind)
		//{
		//	bIsFind = FindTargetInOverlapTargets();
		//}

		if (!bIsFind)
		{
			SetTargetDash(nullptr);
		}
	}
}

bool UDashComponent::FindTargetInOverlapTargets()
{
	if (!OwnerCamera)
	{
		return false;
	}

	if (OverlapTargets.Num() > 0)
	{
		const auto CameraLocation = OwnerCamera->GetComponentLocation();
		float MinDot = 0.5f;
		float MinDistance = TNumericLimits<float>::Max();
		AActor* NewTargetDash = nullptr;

		for (const auto OverlapActor : OverlapTargets)
		{
			/** Direction Camera to OverlapActor: */
			FVector DirCameraToActor = UKismetMathLibrary::GetDirectionUnitVector(GetOwner()->GetActorLocation(), OverlapActor->GetActorLocation());

			/** Dot vectors: Character Forward Vector and Direction to Cursor: */
			float DotCameraToOverlapActor = UKismetMathLibrary::Dot_VectorVector(OwnerCamera->GetForwardVector(), DirCameraToActor);

			if (DotCameraToOverlapActor > MinDot)
			{
				const auto Distance = UKismetMathLibrary::Vector_Distance(GetOwner()->GetActorLocation(), OverlapActor->GetActorLocation());
				if (Distance < MinDistance)
				{
					MinDistance = Distance;
					NewTargetDash = OverlapActor;
				}
			}
		}

		if (NewTargetDash)
		{
			SetTargetDash(NewTargetDash);
			return true;
		}
	}
	return false;
}

void UDashComponent::TargetCollisionBeginOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OverlapTargets.Find(OtherActor) < 0)
	{
		OverlapTargets.Add(OtherActor);
	}
}

void UDashComponent::TargetCollisionEndOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OverlapTargets.Find(OtherActor) > -1)
	{
		OverlapTargets.Remove(OtherActor);
	}
}
