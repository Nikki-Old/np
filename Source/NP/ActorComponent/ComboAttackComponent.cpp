// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/ComboAttackComponent.h"
#include "Animation/AnimFL.h"
#include "Animation/Notify/StartEndAttackAN.h"

#include "FunctionLibrary/NPFuncLib.h"

// Sets default values for this component's properties
UComboAttackComponent::UComboAttackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

bool UComboAttackComponent::Attack(EAttackKey AttackKey, bool bIsForce)
{
	if (AttackKey != EAttackKey::None_Key && OwnerMesh)
	{
		if (!bIsStartAttack || bIsForce)
		{
			// Start attack:
			bIsStartAttack = true;

			// Save Combo number:
			OldComboNumber = ComboNumber;

			// Create sufix for combo attack:

			// TO DO: discard FName and add Combo number in FComboPartInfo struct:

			// Get AnimMontage:
			FComboPartInfo ComboPartInfo;
			if (!GetAnimMontageForCombo(AttackKey, ComboNumber, ComboPartInfo))
			{
				ComboNumber = 0;
				OldComboNumber = ComboNumber;
				GetAnimMontageForCombo(AttackKey, ComboNumber, ComboPartInfo);
			}

			if (ComboPartInfo.Animation)
			{
				auto StartEndNotify = AnimUtils::FindNotifyByClass<UStartEndAttackAN>(ComboPartInfo.Animation);
				checkf(StartEndNotify, TEXT("Animation not have \"UStartEndAttackAN\" animation notify!"));

				// Bind Start End Notify:
				StartEndNotify->OnNotifiedSignature.AddUObject(this, &UComboAttackComponent::StartEndCurrentAnim);

				TArray<UDealDamageAN*> DealDamageNotifies = {};
				if (AnimUtils::FindNotifiesByClass<UDealDamageAN>(ComboPartInfo.Animation, DealDamageNotifies))
				{
					CurrentAnimMontage = ComboPartInfo.Animation;

					for (auto DealDamageNotify : DealDamageNotifies)
					{
						if (StartEndNotify && DealDamageNotify)
						{
							// Bind Deal Damage Notify:
							if (!DealDamageNotify->OnDealDamage.IsAlreadyBound(this, &UComboAttackComponent::DealDamageCurrentAnim))
							{
								DealDamageNotify->OnDealDamage.AddDynamic(this, &UComboAttackComponent::DealDamageCurrentAnim);
							}
						}
					}
					checkf(DealDamageNotifies.Num() > 0, TEXT("Animation not have \"UDealDamageAN\" animation notify!"));

					CurrentAnimInstance = AnimUtils::PlayAnimMontage(OwnerMesh, CurrentAnimMontage, ComboPartInfo.ScaleRate);
					if (CurrentAnimInstance)
					{
						if (!CurrentAnimInstance->OnMontageEnded.IsAlreadyBound(this, &UComboAttackComponent::EndCurrentAnim))
						{
							CurrentAnimInstance->OnMontageEnded.AddDynamic(this, &UComboAttackComponent::EndCurrentAnim);
						}
						OnStartAttack.Broadcast(CurrentAnimMontage, ComboNumber);
					}
				}
				else
				{
					// Reset combo number:
					ComboNumber = 0;
				}
			}
		}
		else
		{
			if (bIsCanAddComboNumber)
			{
				bIsCanAddComboNumber = false;
				ComboNumber++;

				// Save last attack name:
				LastAttackKey = AttackKey;
			}
		}
	}
	return false;
}

void UComboAttackComponent::StopAttack()
{
	if (CurrentAnimInstance)
	{
		CurrentAnimInstance->StopAllMontages(StopBlendOut);
	}
}

// TO DO: take Combo number:
bool UComboAttackComponent::GetAnimMontageForCombo(EAttackKey AttackKey, int CurrentComboNumber, FComboPartInfo& OutInformation)
{
	if (AttackKey == EAttackKey::None_Key)
	{
		return false;
	}

	// Find combo for start:
	if (CurrentComboNumber == 0)
	{
		AvailableComboInformation.Empty();

		for (auto Combo : ComboInformation)
		{
			if (Combo.Combo[0].AttackKey == AttackKey)
			{
				AvailableComboInformation.Add(Combo);
			}
		}

		if (AvailableComboInformation.IsValidIndex(0))
		{
			OutInformation = AvailableComboInformation[0].Combo[0];
			return true;
		}
		else
		{
			return false;
		}
	}

	for (int i = 0; i < AvailableComboInformation.Num(); i++)
	{
		if (AvailableComboInformation[i].Combo.IsValidIndex(CurrentComboNumber))
		{
			if (AvailableComboInformation[i].Combo[CurrentComboNumber].AttackKey == AttackKey)
			{
				OutInformation = AvailableComboInformation[i].Combo[CurrentComboNumber];
				return true;
			}
			else
			{
				AvailableComboInformation.RemoveAt(i--);
			}
		}
	}

	//for (auto AvailableCombo : AvailableComboInformation)
	//{
	//	if (AvailableCombo.Combo.IsValidIndex(CurrentComboNumber))
	//	{
	//		if (AvailableCombo.Combo[CurrentComboNumber].AttackName == ComboName)
	//		{
	//			OutInformation = AvailableCombo.Combo[CurrentComboNumber];
	//			return true;
	//		}
	//	}
	//}

	return false;
}

void UComboAttackComponent::StartEndCurrentAnim(USkeletalMeshComponent* SkeletalMesh)
{
	if (OldComboNumber < ComboNumber)
	{
		// if (CurrentAnimInstance)
		//{
		//	CurrentAnimInstance->OnMontageEnded.RemoveDynamic(this, &UComboAttackComponent::EndCurrentAnim);
		// }

		bIsCanAddComboNumber = true;

		Attack(LastAttackKey, true);
	}
	OnEndAttack.Broadcast();
}

void UComboAttackComponent::DealDamageCurrentAnim(FDealDamageInfo DealDamageInfo)
{
	OnCanDealDamage.Broadcast(DealDamageInfo);
}

void UComboAttackComponent::EndCurrentAnim(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage == CurrentAnimMontage)
	{
		ComboNumber = 0;
		bIsCanAddComboNumber = true;
		bIsStartAttack = false;
	}
	OnEndAttack.Broadcast();
}

// Called when the game starts
void UComboAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...

	// TO DO: maybe it can be done earlier?
	// Get Owner mesh:
	OwnerMesh = UNPFuncLib::GetBodySkeletalMesh(GetOwner());
	checkf(OwnerMesh, TEXT("Not find \"OwnerMesh\", owner mesh component have tag \"Body\"?"));
}

// Called every frame
void UComboAttackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}
