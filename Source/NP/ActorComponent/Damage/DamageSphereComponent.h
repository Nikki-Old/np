// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "DamageSphereComponent.generated.h"

/**
 * 
 */

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnBeginOverlap, FHitResult, HitResult, float, Damage);

UCLASS(ClassGroup = "Collision", editinlinenew, hidecategories = (Object, LOD, Lighting, TextureStreaming), meta = (DisplayName = "Damage Collision", BlueprintSpawnableComponent))
class NP_API UDamageSphereComponent : public USphereComponent
{
	GENERATED_BODY()

public:
	UDamageSphereComponent();

	/** Toggle can deal damage: */
	UFUNCTION(BlueprintCallable, Category = "DamageSphere")
	void ToggleCanDealDamage(bool bIsNewCan, float NewDamage);

	UPROPERTY(BlueprintAssignable, Category = "DamageSphere")
	FOnBeginOverlap OnBeginOverlap;

protected:
	UFUNCTION(BlueprintNativeEvent, Category = "DamageSphere")
	void DamageSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	void DamageSphereBeginOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION(BlueprintNativeEvent, Category = "DamageSphere")
	void DamageSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	void DamageSphereEndOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:
	bool bIsCanDealDamage = false;

	TArray<AActor*> OverlappedActors = {};

	float Damage = 0.0f;
};
