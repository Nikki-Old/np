// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorComponent/Damage/DamageSphereComponent.h"
#include "Engine/EngineTypes.h"

UDamageSphereComponent::UDamageSphereComponent()
{
	this->OnComponentBeginOverlap.AddDynamic(this, &UDamageSphereComponent::DamageSphereBeginOverlap);
	this->OnComponentEndOverlap.AddDynamic(this, &UDamageSphereComponent::DamageSphereEndOverlap);
}

void UDamageSphereComponent::ToggleCanDealDamage(bool bIsNewCan, float NewDamage)
{
	bIsCanDealDamage = bIsNewCan;
	Damage = NewDamage;
	ECollisionEnabled::Type Collision = bIsCanDealDamage ? ECollisionEnabled::QueryAndPhysics : ECollisionEnabled::NoCollision;
	this->SetCollisionEnabled(Collision);

	if (!bIsCanDealDamage)
	{
		OverlappedActors.Empty();
	}
}

void UDamageSphereComponent::DamageSphereBeginOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OverlappedActors.Find(OtherActor) == -1 && OtherActor != GetOwner())
	{
		OverlappedActors.Add(OtherActor);
		OnBeginOverlap.Broadcast(SweepResult, Damage);
		ToggleCanDealDamage(false, 0.0f);
	}
}

void UDamageSphereComponent::DamageSphereEndOverlap_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//if (OverlappedActors.Find(OtherActor) > 0 && OtherActor != GetOwner())
	//{
	//	OverlappedActors.Remove(OtherActor);
	//}
}
