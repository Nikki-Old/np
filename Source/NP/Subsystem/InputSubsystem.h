// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "InputSubsystem.generated.h"

/**
 *
 */

USTRUCT(BlueprintType)
struct FNPInputInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "NPInputInfo")
	FName InputName = FName();

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "NPInputInfo")
	bool bIsPressed = false;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSendInput, const FNPInputInfo&, NPInputInfo);

UCLASS()
class NP_API UInputSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintAssignable, Category = "InputSubsystem")
	FOnSendInput OnSendInput;

	UFUNCTION(BlueprintCallable, Category = "InputSubsystem")
	void SendInput(const FNPInputInfo& NewInputInfo);
};
