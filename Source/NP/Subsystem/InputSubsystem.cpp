// Fill out your copyright notice in the Description page of Project Settings.


#include "Subsystem/InputSubsystem.h"

void UInputSubsystem::SendInput(const FNPInputInfo& NewInputInfo)
{
	OnSendInput.Broadcast(NewInputInfo);
}
