// Fill out your copyright notice in the Description page of Project Settings.

#include "Controller/PlayerController/NPPlayerController.h"
#include "Character/PlayerCharacter.h"
#include "GameFramework/GameModeBase.h"
#include "Blueprint/WidgetBlueprintLibrary.h"

#include "Interface/GameModeInteract.h"
#include "UI/NPHUD.h"

void ANPPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);

	if (GetPawn())
	{
		if (PlayerCharacter != aPawn)
		{
			PlayerCharacter = Cast<APlayerCharacter>(aPawn);

			SetupInputComponent();

			// Send message to Game mode, if PlayerPawn is ready:
			auto CurrentGameMode = GetWorld()->GetAuthGameMode();
			if (CurrentGameMode)
			{
				// Is have Interface implementation:
				if (CurrentGameMode->GetClass()->ImplementsInterface(UGameModeInteract::StaticClass()))
				{
					IGameModeInteract::Execute_SetPlayerPawn(CurrentGameMode, this, PlayerCharacter);
				}
			}
		}
	}
}

void ANPPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	if (InputComponent && PlayerCharacter)
	{
		// Rotation:
		InputComponent->BindAxis(TEXT("LookUpRate"), PlayerCharacter, &APlayerCharacter::LookUpRate);
		InputComponent->BindAxis(TEXT("LookUp"), PlayerCharacter, &APlayerCharacter::LookUp);

		InputComponent->BindAxis(TEXT("TurnRate"), PlayerCharacter, &APlayerCharacter::TurnRate);
		InputComponent->BindAxis(TEXT("Turn"), PlayerCharacter, &APlayerCharacter::Turn);

		// Direction movement:
		InputComponent->BindAxis(TEXT("MoveForward"), PlayerCharacter, &APlayerCharacter::MoveForward);
		InputComponent->BindAxis(TEXT("MoveRight"), PlayerCharacter, &APlayerCharacter::MoveRight);

		// Jump:
		InputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, PlayerCharacter, &ACharacter::Jump);
		InputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Released, PlayerCharacter, &ACharacter::StopJumping);

		// Dash:
		InputComponent->BindAction(TEXT("DashInput"), EInputEvent::IE_Pressed, PlayerCharacter, &APlayerCharacter::TryDash);

		// Pause:
		InputComponent->BindAction(TEXT("Pause"), EInputEvent::IE_Pressed, this, &ANPPlayerController::TogglePause).bExecuteWhenPaused = true;

		// Attack:
		InputComponent->BindAction(TEXT("LightAttack"), EInputEvent::IE_Pressed, PlayerCharacter, &APlayerCharacter::TryLightAttack);
		InputComponent->BindAction(TEXT("HeavyAttack"), EInputEvent::IE_Pressed, PlayerCharacter, &APlayerCharacter::TryHeavyAttack);
	}
}

void ANPPlayerController::TogglePause()
{
	auto CurrentGameMode = GetWorld()->GetAuthGameMode();
	if (CurrentGameMode)
	{
		// Is have Interface implementation:
		if (CurrentGameMode->GetClass()->ImplementsInterface(UGameModeInteract::StaticClass()))
		{
			IGameModeInteract::Execute_SetGamePause(CurrentGameMode, this);
		}
	}
}

EInputType ANPPlayerController::GetInputMode_Implementation()
{
	return CurrentInputType;
}

void ANPPlayerController::SetInputModeByType_Implementation(EInputType NewType, UWidget* WidgetFocus)
{
	if (CurrentInputType == NewType)
	{
		return;
	}

	CurrentInputType = NewType;

	switch (CurrentInputType)
	{
		case EInputType::GameOnly_Type:
		{
			this->bShowMouseCursor = false;
			UWidgetBlueprintLibrary::SetInputMode_GameOnly(this);
		}
		break;

		case EInputType::UIOnly_Type:
		{
			this->bShowMouseCursor = true;
			UWidgetBlueprintLibrary::SetInputMode_UIOnly(this, WidgetFocus, true);
		}
		break;

		case EInputType::GameUI_Type:
		{
			UWidgetBlueprintLibrary::SetInputMode_GameAndUI(this, WidgetFocus, true);
		}
		break;

		default:
			break;
	}
}

ANPHUD* ANPPlayerController::GetNPHUD_Implementation()
{
	if (!NPHUD)
	{
		NPHUD = GetHUD<ANPHUD>();
	}
	
	return NPHUD;
}
