// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FunctionLibrary/NPTypes.h"
#include "Interface/NPPlayerControllerInteface.h"
#include "NPPlayerController.generated.h"

/**
 *
 */

class APlayerCharacter;
class ANPHUD;

UCLASS()
class NP_API ANPPlayerController : public APlayerController, public INPPlayerControllerInteface
{
	GENERATED_BODY()
public:


	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "NPPlayerController")
	bool IsPaused() const { return bIsPaused; }

	UFUNCTION(BlueprintCallable, Category = "NPPlayerController")
	void SetPaused(bool bIsNewPaused) { bIsPaused = bIsNewPaused; }

#pragma region INPPlayerControllerInteface
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPPlayerControllerInterface")
	ANPHUD* GetNPHUD();
	ANPHUD* GetNPHUD_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPPlayerControllerInterface")
	EInputType GetInputMode();
	EInputType GetInputMode_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPPlayerControllerInterface")
	void SetInputModeByType(EInputType NewType, UWidget* WidgetFocus = nullptr);
	void SetInputModeByType_Implementation(EInputType NewType, UWidget* WidgetFocus = nullptr);
#pragma endregion

protected:
	virtual void OnPossess(APawn* aPawn) override;
	virtual void SetupInputComponent() override;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly, Category = "NPPlayerController")
	APlayerCharacter* PlayerCharacter = nullptr;

	UFUNCTION(BlueprintCallable, Category = "NPPlayerController")
	void TogglePause();

private:
	EInputType CurrentInputType = EInputType::GameOnly_Type;

	bool bIsPaused = false;

	ANPHUD* NPHUD = nullptr;
};