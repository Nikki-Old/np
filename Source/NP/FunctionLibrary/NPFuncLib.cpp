// Fill out your copyright notice in the Description page of Project Settings.

#include "FunctionLibrary/NPFuncLib.h"
#include "Engine/LatentActionManager.h"
#include "LatentActions.h"
#include "Interface/GameModeInteract.h"
#include "GameFramework/GameModeBase.h"
#include "GameMode/NPGameModeBase.h"

USkeletalMeshComponent* UNPFuncLib::GetBodySkeletalMesh(AActor* Target)
{
	if (Target)
	{
		const auto Meshs = Target->GetComponentsByTag(UMeshComponent::StaticClass(), "Body");

		if (Meshs.IsValidIndex(0))
		{
			const auto SkeletalMesh = Cast<USkeletalMeshComponent>(Meshs[0]);
			if (SkeletalMesh)
			{
				return SkeletalMesh;
			}
		}
	}

	return nullptr;
}

class FPlayerWaitAction : public FPendingLatentAction
{
public:
	AGameModeBase* GameMode = nullptr;
	ANPPlayerController* PlayerController = nullptr;
	APawn*& PlayerPawnRef;
	FName ExecutionFunction;
	int32 OutputLink;
	FWeakObjectPtr CallbackTarget;

	/**
	 * @param DialogComponent - Pointer to DialogComponent that called this function.
	 * @param LatentInfo - latent info for callback
	 */
	FPlayerWaitAction(AGameModeBase* GameMode, ANPPlayerController* PlayerController, APawn*& PlayerPawnRef, const FLatentActionInfo& LatentInfo)
		: GameMode(GameMode), PlayerController(PlayerController), PlayerPawnRef(PlayerPawnRef), ExecutionFunction(LatentInfo.ExecutionFunction), OutputLink(LatentInfo.Linkage), CallbackTarget(LatentInfo.CallbackTarget) {}

	virtual void UpdateOperation(FLatentResponse& Response) override
	{
		if (GameMode)
		{
			// Is have Interface implementation:
			if (GameMode->GetClass()->ImplementsInterface(UGameModeInteract::StaticClass()))
			{
				PlayerPawnRef = IGameModeInteract::Execute_GetPlayerPawnByController(GameMode, PlayerController);
				Response.FinishAndTriggerIf(PlayerPawnRef != nullptr, ExecutionFunction, OutputLink, CallbackTarget);
			}
		}
	}
};

// TO DO: recreate Latent function:
void UNPFuncLib::WaitPlayerPawn(const UObject* World, ANPPlayerController* PlayerController, APawn*& PlayerPawn, struct FLatentActionInfo LatentInfo)
{
	if (World && PlayerController)
	{
		auto WorldContext = GEngine->GetWorldFromContextObject(World, EGetWorldErrorMode::LogAndReturnNull);
		// Prepare latent action
		if (WorldContext)
		{
			FLatentActionManager& LatentActionManager = WorldContext->GetLatentActionManager();
			if (LatentActionManager.FindExistingAction<FPlayerWaitAction>(LatentInfo.CallbackTarget, LatentInfo.UUID) == NULL)
			{
				auto GameMode = WorldContext->GetAuthGameMode();
				LatentActionManager.AddNewAction(LatentInfo.CallbackTarget, LatentInfo.UUID, new FPlayerWaitAction(GameMode, PlayerController, PlayerPawn, LatentInfo));
			}
		}
	}
}

ANPGameModeBase* UNPFuncLib::GetNRGameMode(const UObject* World)
{
	if (World)
	{
		auto WorldContext = GEngine->GetWorldFromContextObject(World, EGetWorldErrorMode::LogAndReturnNull);
		if (WorldContext)
		{
			auto GameMode = WorldContext->GetAuthGameMode();
			if (GameMode)
			{
				const auto NRGameMode = Cast<ANPGameModeBase>(GameMode);
				return NRGameMode;
			}
		}
	}
	return nullptr;
}
