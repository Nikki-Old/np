// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "NPTypes.generated.h"

/**
 *
 */

UENUM(BlueprintType)
enum class EAttackKey : uint8
{
	None_Key UMETA(DisplayName = "None"),
	LightAttack_Key UMETA(DisplayName = "LightAttack"),
	HeavyAttack_Key UMETA(DisplayName = "HeavyAttack")
};

UENUM(BlueprintType)
enum class EInputType : uint8
{
	GameOnly_Type UMETA(DisplayName = "GameOnly"),
	UIOnly_Type UMETA(DisplayName = "UIOnly"),
	GameUI_Type UMETA(DisplayName = "GameUI"),
};


UCLASS()
class NP_API UNPTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};
