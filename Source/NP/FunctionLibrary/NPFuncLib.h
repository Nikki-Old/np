// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "NPFuncLib.generated.h"

/**
 *
 */

class USkeletalMeshComponent;
class ANPGameModeBase;
class ANPPlayerController;

UCLASS()
class NP_API UNPFuncLib : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "NRFunctionLibrary")
	static USkeletalMeshComponent* GetBodySkeletalMesh(AActor* Target);

	/** Send Player Pawn after spawn Player: */
	UFUNCTION(BlueprintCallable, Category = "NRFunctionLibrary", meta = (WorldContext = "World", Latent, LatentInfo = "LatentInfo"))
	static void WaitPlayerPawn(const UObject* World, ANPPlayerController* PlayerController, APawn*& PlayerPawn, struct FLatentActionInfo LatentInfo);


	UFUNCTION(BlueprintCallable, Category = "NRFunctionLibrary", meta = (WorldContext = "World"))
	static ANPGameModeBase* GetNRGameMode(const UObject* World);
};
