// Fill out your copyright notice in the Description page of Project Settings.

#include "Actor/SpawnActor/SpawnActor.h"
#include "Components/BillboardComponent.h"
#include "Engine/TargetPoint.h"

// Sets default values
ASpawnActor::ASpawnActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create bilboard and set root component:
	BillboardComp = CreateDefaultSubobject<UBillboardComponent>(TEXT("BillboardImage"));
	this->SetRootComponent(BillboardComp);
}

void ASpawnActor::StartSpawnActors_Implementation(APawn* PawnInstigator)
{
	for (auto SpawnInfo : SpawnActorsInfo)
	{
		const auto NewActor = SpawnActor(PawnInstigator, SpawnInfo);

		if (NewActor)
		{
			SpawnInfo.SpawnedActor = NewActor;
		}
	}
}

AActor* ASpawnActor::SpawnActor_Implementation(APawn* PawnInstigator, FSpawnActorInfo SpawnInfo)
{
	if (!GetWorld())
	{
		return nullptr;
	}

	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.Owner = this;
	ActorSpawnParameters.Instigator = PawnInstigator;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	if (SpawnInfo.TargetPoint)
	{
		SpawnInfo.SpawnTransform = SpawnInfo.TargetPoint->GetActorTransform();
	}

	auto NewActor = GetWorld()->SpawnActor<AActor>(SpawnInfo.ActorClass, SpawnInfo.SpawnTransform, ActorSpawnParameters);

	if (NewActor)
	{
		NewActor->OnDestroyed.AddDynamic(this, &ASpawnActor::SpawnedActorOnDestroed);
		return NewActor;
	}

	return nullptr;
}

// Called when the game starts or when spawned
void ASpawnActor::BeginPlay()
{
	Super::BeginPlay();
}

void ASpawnActor::SpawnedActorOnDestroed_Implementation(AActor* DestroyedActor)
{
	for (auto SpawnInfo : SpawnActorsInfo)
	{
		if (SpawnInfo.SpawnedActor == DestroyedActor)
		{
			OnDestroedSpawnedActor.Broadcast(SpawnInfo);
		}
	}
}

// Called every frame
void ASpawnActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
