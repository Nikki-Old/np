// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnActor.generated.h"

class UBillboardComponent;
class ATargetPoint;

USTRUCT(BlueprintType)
struct FSpawnActorInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SpawnActorInformation")
	TSubclassOf<AActor> ActorClass = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SpawnActorInformation")
	ATargetPoint* TargetPoint = nullptr;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "SpawnActorInformation")
	FTransform SpawnTransform = FTransform();

	UPROPERTY(BlueprintReadOnly, Category = "SpawnActorInformation")
	AActor* SpawnedActor = nullptr;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDestroedSpawnedActor, FSpawnActorInfo, SpawnActorInfo);

UCLASS()
class NP_API ASpawnActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ASpawnActor();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "SpawnActor")
	void StartSpawnActors(APawn* PawnInstigator);
	void StartSpawnActors_Implementation(APawn* PawnInstigator);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "SpawnActor")
	AActor* SpawnActor(APawn* PawnInstigator, FSpawnActorInfo SpawnInfo);
	virtual AActor* SpawnActor_Implementation(APawn* PawnInstigator, FSpawnActorInfo SpawnInfo);

	UFUNCTION(BlueprintCallable, Category = "SpawnActor")
	void SetSpawnActorInfo(TArray<FSpawnActorInfo> NewInfromation) { SpawnActorsInfo = NewInfromation; }

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "SpawnActor")
	TArray<FSpawnActorInfo> GetSpawnActorInfo() const { return SpawnActorsInfo; }

	UPROPERTY(BlueprintAssignable, Category = "SpawnActor")
	FOnDestroedSpawnedActor OnDestroedSpawnedActor;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "SpawnActor")
	TArray<FSpawnActorInfo> SpawnActorsInfo = {};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "SpawnActor")
	UBillboardComponent* BillboardComp = nullptr;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "SpawnActor")
	void SpawnedActorOnDestroed(AActor* DestroyedActor);
	virtual void SpawnedActorOnDestroed_Implementation(AActor* DestroyedActor);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
