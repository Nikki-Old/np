// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "PlayerCharacter.generated.h"

class USpringArmComponent;
class UCameraComponent;
class UDashComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNewFocus, AActor*, FocusTarget);

UCLASS()
class NP_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerCharacter(const FObjectInitializer& ObjectInitializer);

	// Camera components:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PlayerCharacter")
	USpringArmComponent* SpringArm = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PlayerCharacter")
	UCameraComponent* Camera = nullptr;

	// Controll Dash component:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PlayerCharacter")
	UDashComponent* DashComponent = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

#pragma region Movement
public:
	UFUNCTION(BlueprintCallable, Category = "PlayerCharacter")
	void SetCanMove(bool bIsNewCan) { bIsCanMove = bIsNewCan; }

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerCharacter | Movement")
	float BaseTurnRate = 45.0f;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "PlayerCharacter | Movement")
	float BaseLookUpRate = 45.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PlayerCharacter | Movement")
	bool bIsCameraDirection = true;

public:
	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharacter | Dash")
	void TryDash();
	void TryDash_Implementation();

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharacter | Movement")
	void LookUpRate(float Axis);
	void LookUpRate_Implementation(float Axis);

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharacter | Movement")
	void LookUp(float Axis);
	void LookUp_Implementation(float Axis);

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharacter | Movement")
	void TurnRate(float Axis);
	void TurnRate_Implementation(float Axis);

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharacter | Movement")
	void Turn(float Axis);
	void Turn_Implementation(float Axis);

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharacter | Movement")
	void MoveForward(float Axis);
	void MoveForward_Implementation(float Axis);

	UFUNCTION(BlueprintNativeEvent, Category = "PlayerCharacter | Movement")
	void MoveRight(float Axis);
	void MoveRight_Implementation(float Axis);
#pragma endregion

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	bool bIsFirstDash = true;
	bool bIsCanMove = true;

#pragma region CameraFocus
public:
	/** Set camera focus by target: */
	UFUNCTION(BlueprintCallable, Category = "PlayerCharacter | CameraFocus")
	void SetCameraFocusByTarget(AActor* NewCameraFocusTarget, bool bIsLockCamera = false);

	/** Clear and stop - camera focus by target: */
	UFUNCTION(BlueprintCallable, Category = "PlayerCharacter | CameraFocus")
	void ClearCameraFocusTarget();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "PlayerCharacter | CameraFocus")
	bool IsHaveFocus() const { return bIsHaveFocus; }

	UPROPERTY(BlueprintAssignable, Category = "PlayerCharacter | CameraFocus")
	FOnNewFocus OnNewFocus;

protected:
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "PlayerCharacter | CameraFocus")
	bool bIsNeedLockCameraInput = false;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "PlayerCharacter | CameraFocus")
	float CameraOffset = 65.0f;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "PlayerCharacter | CameraFocus")
	float InterpSpeed = 1.0f;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "PlayerCharacter | CameraFocus")
	float HowOftenUpdateCameraFocus = 0.01f;

	UFUNCTION()
	void UpdateCameraFocus();

	UFUNCTION()
	void SetPausedUpdateCameraFocus(bool bIsPaused);

private:
	/** Timer for set Camera focus */
	FTimerHandle CameraFocusTimer;

	bool bIsHaveFocus = false;

	bool bIsLockCameraInput = false;

	UPROPERTY()
	AActor* CurrentCameraFocusTarget = nullptr;
#pragma endregion

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PlayerCharacter")
	void TryLightAttack();
	void TryLightAttack_Implementation();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PlayerCharacter")
	void TryHeavyAttack();
	void TryHeavyAttack_Implementation();
};