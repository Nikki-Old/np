// Fill out your copyright notice in the Description page of Project Settings.

#include "PlayerCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "ActorComponent/Movement/NRCharacterMovementComponent.h"
#include "ActorComponent/Movement/DashComponent.h"

// Sets default values
APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UNRCharacterMovementComponent>(APlayerCharacter::CharacterMovementComponentName))
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false; // Tick is OFF!

	this->bUseControllerRotationYaw = false;
	// Create defaults components:
	// Add spring arm and Camera:
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->SetupAttachment(GetRootComponent());
	SpringArm->bDoCollisionTest = false; // Off Do Collision Test
	SpringArm->bUsePawnControlRotation = true;

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("PlayerCamera"));
	Camera->SetupAttachment(SpringArm, "SpringEndpoint");
	Camera->ComponentTags.Add("MainCamera");

	DashComponent = CreateDefaultSubobject<UDashComponent>(TEXT("DashComponent"));
}

void APlayerCharacter::SetCameraFocusByTarget(AActor* NewCameraFocusTarget, bool bIsLockCamera)
{
	if (NewCameraFocusTarget && CurrentCameraFocusTarget != NewCameraFocusTarget)
	{
		CurrentCameraFocusTarget = NewCameraFocusTarget;

		GetWorld()->GetTimerManager().SetTimer(CameraFocusTimer, this, &APlayerCharacter::UpdateCameraFocus, HowOftenUpdateCameraFocus, true);
		bIsHaveFocus = true;
		OnNewFocus.Broadcast(CurrentCameraFocusTarget);

		if (bIsNeedLockCameraInput)
		{
			bIsLockCameraInput = bIsLockCamera;
		}
	}
}

void APlayerCharacter::ClearCameraFocusTarget()
{
	bIsHaveFocus = false;
	bIsLockCameraInput = false;
	CurrentCameraFocusTarget = nullptr;
	OnNewFocus.Broadcast(CurrentCameraFocusTarget);
	GetWorld()->GetTimerManager().ClearTimer(CameraFocusTimer);
}

void APlayerCharacter::UpdateCameraFocus()
{
	auto PlayerController = GetController();
	if (CurrentCameraFocusTarget && Camera && PlayerController)
	{
		auto ControlRotation = PlayerController->GetControlRotation();

		auto TargetLocation = CurrentCameraFocusTarget->GetActorLocation();
		TargetLocation.Z -= CameraOffset;
		auto FindLookAtRotation = UKismetMathLibrary::FindLookAtRotation(Camera->GetComponentLocation(), TargetLocation);

		FRotator RInterp = UKismetMathLibrary::RInterpTo(ControlRotation, FindLookAtRotation, 0.001f, InterpSpeed);

		PlayerController->SetControlRotation(FRotator(RInterp.Pitch, RInterp.Yaw, ControlRotation.Roll));
	}
}

void APlayerCharacter::SetPausedUpdateCameraFocus(bool bIsPaused)
{
	if (bIsPaused)
	{
		GetWorld()->GetTimerManager().PauseTimer(CameraFocusTimer);
	}
	else
	{
		GetWorld()->GetTimerManager().UnPauseTimer(CameraFocusTimer);
	}
}

void APlayerCharacter::TryLightAttack_Implementation()
{
	// BP:
}

void APlayerCharacter::TryHeavyAttack_Implementation()
{
	// BP:
}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
	DashComponent->SetFindTargetForDash(true);
}

void APlayerCharacter::TryDash_Implementation()
{
	if (DashComponent)
	{
		DashComponent->Dash();
	}
}

void APlayerCharacter::LookUpRate_Implementation(float Axis)
{
	// Gamepad:
	if (Axis != 0.0f && !bIsLockCameraInput)
	{
		if (IsHaveFocus())
		{
			SetPausedUpdateCameraFocus(true);
		}

		float NewValue = Axis * BaseTurnRate * GetWorld()->GetDeltaSeconds();
		AddControllerPitchInput(NewValue);
	}
	else if (IsHaveFocus() && !bIsLockCameraInput)
	{
		SetPausedUpdateCameraFocus(false);
	}
}

void APlayerCharacter::LookUp_Implementation(float Axis)
{
	// Mouse:
	if (Axis != 0.0f && !bIsLockCameraInput)
	{
		if (IsHaveFocus())
		{
			SetPausedUpdateCameraFocus(true);
		}
		AddControllerPitchInput(Axis);
	}
	else if (IsHaveFocus() && !bIsLockCameraInput)
	{
		SetPausedUpdateCameraFocus(false);
	}
}

void APlayerCharacter::TurnRate_Implementation(float Axis)
{
	// Gamepad:
	if (Axis != 0.0f && !bIsLockCameraInput)
	{
		if (IsHaveFocus())
		{
			SetPausedUpdateCameraFocus(true);
		}

		float NewValue = Axis * BaseLookUpRate * GetWorld()->GetDeltaSeconds();
		AddControllerPitchInput(NewValue);
	}
	else if (IsHaveFocus() && !bIsLockCameraInput)
	{
		SetPausedUpdateCameraFocus(false);
	}
}

void APlayerCharacter::Turn_Implementation(float Axis)
{
	// Mouse:
	if (Axis != 0.0f && !bIsLockCameraInput)
	{
		if (IsHaveFocus())
		{
			SetPausedUpdateCameraFocus(true);
		}
		AddControllerYawInput(Axis);
	}
	else if (IsHaveFocus() && !bIsLockCameraInput)
	{
		SetPausedUpdateCameraFocus(false);
	}
}

void APlayerCharacter::MoveForward_Implementation(float Axis)
{
	if (Axis != 0.0f && Camera && bIsCanMove)
	{
		FVector Direction;

		if (bIsCameraDirection)
		{
			auto CameraFV = Camera->GetForwardVector();
			CameraFV.Z = 0.0f;
			Direction = CameraFV * Axis;
		}
		else
		{
			Direction = this->GetActorForwardVector() * Axis;
		}

		this->AddMovementInput(Direction, true);
	}
}

void APlayerCharacter::MoveRight_Implementation(float Axis)
{
	if (Axis != 0.0f && Camera && bIsCanMove)
	{
		FVector Direction;

		if (bIsCameraDirection)
		{
			Direction = Camera->GetRightVector() * Axis;
		}
		else
		{
			Direction = this->GetActorRightVector() * Axis;
		}

		this->AddMovementInput(Direction, true);
	}
}

// Called every frame
void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}
