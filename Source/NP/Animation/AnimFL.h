#pragma once

class AnimUtils
{
public:
	template <typename T>
	static bool FindNotifiesByClass(UAnimSequenceBase* Animation, TArray<T*>& OutInfo)
	{
		if (!Animation)
			return nullptr;

		TArray<T*> NeededNotify = {};

		// Take array Notify:
		const auto NotifyEvents = Animation->Notifies;
		for (auto NotifyEvent : NotifyEvents) //...search Notify event in Notify array...
		{
			auto AnimNotify = Cast<T>(NotifyEvent.Notify);
			if (AnimNotify)
			{
				NeededNotify.Add(AnimNotify);
			}
		}

		OutInfo = NeededNotify;
		return OutInfo.Num() > 0;
	}

	template <typename T>
	static T* FindNotifyByClass(UAnimSequenceBase* Animation)
	{
		if (!Animation)
			return nullptr;

		// Take array Notify:
		const auto NotifyEvents = Animation->Notifies;
		for (auto NotifyEvent : NotifyEvents) //...search Notify event in Notify array...
		{
			auto AnimNotify = Cast<T>(NotifyEvent.Notify);
			if (AnimNotify)
			{
				return AnimNotify;
			}
		}

		return nullptr;
	}

	static UAnimInstance* PlayAnimMontage(USkeletalMeshComponent* SkeletalMesh, UAnimMontage* AnimMontage, float InPlayRate = 1.0f)
	{
		if (SkeletalMesh && AnimMontage)
		{
			UAnimInstance* AnimInstance = SkeletalMesh->GetAnimInstance();
			if (AnimInstance)
			{
				float Duration = AnimInstance->Montage_Play(AnimMontage, InPlayRate);

				if (Duration > 0.0f)
				{
					return AnimInstance;
				}
			}
		}
		return nullptr;
	}
};