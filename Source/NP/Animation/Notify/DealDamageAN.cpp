// Fill out your copyright notice in the Description page of Project Settings.


#include "Animation/Notify/DealDamageAN.h"

void UDealDamageAN::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	OnDealDamage.Broadcast(DealDamageInfo);
}
