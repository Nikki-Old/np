// Fill out your copyright notice in the Description page of Project Settings.


#include "Animation/Notify/NRAnimNotify.h"

void UNRAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	OnNotifiedSignature.Broadcast(MeshComp);
	Super::Notify(MeshComp, Animation);
}
