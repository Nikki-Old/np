// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "NRAnimNotify.generated.h"

/**
 * 
 */

/** Create delegate for notification: */
DECLARE_MULTICAST_DELEGATE_OneParam(FOnNotifiedSignature, USkeletalMeshComponent*);

UCLASS()
class NP_API UNRAnimNotify : public UAnimNotify
{
	GENERATED_BODY()
public:
	/** Delegate notification:*/
	FOnNotifiedSignature OnNotifiedSignature;

	/** Create virtual function: */
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
};
