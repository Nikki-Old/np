// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/Notify/NRAnimNotify.h"
#include "DealDamageAN.generated.h"

/**
 *
 */
UENUM(BlueprintType)
enum class EBodyPartAttack : uint8
{
	None_Part UMETA(DisplayName = "None"),
	LeftHand_Part UMETA(DisplayName = "LeftHand"),
	RightHand_Part UMETA(DisplayName = "RightHand"),
	LeftLeg_Part UMETA(DisplayName = "LeftLeg"),
	RightLeg_Part UMETA(DisplayName = "RightLeg")
};

USTRUCT(BlueprintType)
struct FDealDamageInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DealDamage")
	float Damage = 0.0f;

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "DealDamage")
	EBodyPartAttack BodyPartAttack = EBodyPartAttack::None_Part;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnDealDamage, FDealDamageInfo, DealDamageInfo);

UCLASS()
class NP_API UDealDamageAN : public UNRAnimNotify
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintAssignable, Category = "DealDamage")
	FOnDealDamage OnDealDamage;

	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

protected:
	UPROPERTY(EditAnyWhere, BlueprintReadOnly, Category = "DealDamage")
	FDealDamageInfo DealDamageInfo = FDealDamageInfo();
};
