// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "GameModeInteract.generated.h"

class ANPPlayerController;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UGameModeInteract : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */

class NP_API IGameModeInteract
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeInteract")
	APawn* GetPlayerPawnByController(const ANPPlayerController* PlayerController);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeInteract")
	void SetPlayerPawn(ANPPlayerController* PlayerController, APawn* NewPlayerpawn);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "GameModeInteract")
	void SetGamePause(ANPPlayerController* PlayerController);
};
