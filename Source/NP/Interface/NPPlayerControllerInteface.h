// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "FunctionLibrary/NPTypes.h"
#include "NPPlayerControllerInteface.generated.h"

class ANPHUD;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UNPPlayerControllerInteface : public UInterface
{
	GENERATED_BODY()
};

/**
 *
 */


class NP_API INPPlayerControllerInteface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPPlayerControllerInterface")
	ANPHUD* GetNPHUD();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPPlayerControllerInterface")
	void SetInputModeByType(EInputType NewInputType, UWidget* WidgetFocus = nullptr);

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "NPPlayerControllerInterface")
	EInputType GetInputMode();
};
